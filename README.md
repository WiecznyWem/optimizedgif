
# Optimized Gif
Optimized Gif, a C++ library, specializes in exporting file size efficient animations. It has a Mathematica interface. 

![Optimized Gif](https://i.imgur.com/63iKJyH.gif)

## Why use it
Optimized Gif library has very efficient GIF optimization algorithm that reduces file size of GIF. Firstly, when two similar images are given, only difference about them is stored. Secondly, the difference is manipulated in a way that reduces usage of LZW codes when image is encoded. Similar algorithm offers GIMP, however, ours implementation supports optimization between rows.

The library is useful when one need to generate an animation that they could share over internet. Unoptimized GIF files can be very large and therefore could be prohibited to upload on some platforms. A good solution, but time consuming, was to generate list of images on your disk, import them into GIF, optimize them for animation, set up delays times, and render it. Using the Optimized Gif library you can automate this process. One would not have to spend 5 minutes to preview his animation every change.  


## Installation
```
mkdir build
cd build
cmake ..
make
make install
```
## CMake optional parameters
* `-DBUILD_SHARED_LIBS=YES`
    * Builds shared version of library. By default static library is builded. 
* `-DCMAKE_INSTALL_PREFIX=<dir>`
    * Specifies where the library should be installed
* `-DMATHEMATICA_PACKAGES_DIR=<dir>`
    * Specifies a directory where a Mathematica package should be installed
    * For Windows generally it is `C:\Users\<User>\AppData\Roaming\Mathematica\Applications`
    * For Linux `/home/<user>/.Mathematica/Applications`
* `-DOpenCL_ENABLED=ON`
    * Enables GPU acceleration
    * On Windows it requires setting up `-DOpenCL_INCLUDE_DIR`, which can be downloaded from [here](https://github.com/KhronosGroup/OpenCL-Headers), and `-DOpenCL_DLL_PATH` which you should find in folder with graphics card drivers.

## Prerequisites on MS Windows
1. Install [MinGW 64](https://sourceforge.net/projects/mingw-w64/) and add its bin folder to your PATH variable. 
2. Install [Cmake 3.12](https://cmake.org/download/)
3. Install [make](http://gnuwin32.sourceforge.net/packages/make.htm)  
3. (optional) Add Mathematica location to your PATH variable, for example 
`C:∖Program Files∖Wolfram Research∖Mathematica\Mathematica\11.3\`.
4. Specify MinGW for cmake:  `cmake .. -G "MinGW Makefiles"`

Building with Visual Studio has not beed tested yet.


## Limitations

Time between frames are measured in centiseconds(`1/100` of second). Values of `0` and `1` centiseconds are generally not supported all over the web. Values between `2` - `5` centiseconds might not be supported on older devices.

Current color quantization algorithm is not optimal in view of performance and quality. It is recommended to use images that have less than 256 colors, so the library can use exact color specified by user. Otherwise, the library will run KMeans quantization to reduce number of colors in given image. This is why usage of Mathematica's `ColorQuantize` gives a lot of quality.

Due to limitations of Mathematica, path to MinGW should **not** contain any space character. 

It is recommended to use `3`, `7`, `15`, `31`, `63`, `127`, or `255` colors, to fully use potential of Color Table. Values are not powers of two, because one color is reserved for optimization purposes.

## C++ Example
``````
#include <iostream>
#include <vector>
#include "OptimizedGif.h"

int main()
{
    const int width = 64;
    const int height = 64;
    const int size = width * height * 3;

    std::vector<unsigned char> frame(size);
    for(int i = 0; i < size; i++)
    {
        int y = i / ( 3* width);
        int x = (i/3) % ( width);
        int k = i % 3;

        if(x == y)
        {
            frame[i] = 0xFF;
        }
        else
        {
            frame[i] = 0x00;
        }
            
    }

    OptimizedGif gif("image.gif", width, height);
    gif.addFrame(frame.data());
    gif.close();
}
``````
And then execute
``````
g++ main.cpp -lOptimizedGif 
./a.out
``````
## Mathematica Example
``````
<<OptimizedGif`

width = 987;
height = 610;
colors = 233;
gif = gifInit["animation.gif", width, height, colors];

For[i = 1,i < 25,i++,
	img = RemoveAlphaChannel[
		Image[
			Plot3D[
				Sin[i/10*x+y]*Cos[i/10*y-x]/(i+x^2+y^2), 
				{x, -Pi, Pi}, {y, -Pi, Pi}, 
				Background->White, 
				AspectRatio->1, 
				PlotRange->{-0.5,0.5}
			], 
			ColorSpace->"RGB", 
			ImageSize->{width, height}
		]
	];
	gifAddFrameCs[gif, img, 5];
];
gifClose[gif];

``````


## C++ Documentation
### OptimizedGif(std::string filename, int width, int height, unsigned short colors = 127, bool looped = true)
initiates a gil file with specified parameters. 
### addFrame(const byte* frame, unsigned int cs = 10)
appends specified frame to gif. Frame should be RGB image without alpha channel. Sets default delay of 10 centiseconds.
### close()
finishes writing to gif file. Mandatory. 

## Mathematica Documentation
### gifInit[filename, width, height, colors]
initializes a gif file with specified name. Returns identifier, required in other functions.
### gitAddFrame[gifId, frame]
appends specified frame to gif with specified identifier. Frame should be RGB image without alpha channel. Sets delay of 10 centiseconds.
### gitAddFrameCs[gifId, frame, delay]
same as gitAddFrame, only you can specify in centiseconds.
### gitClose[gifId]
finishes writing to gif file. Mandatory. 

