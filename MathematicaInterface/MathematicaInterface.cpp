#include "WolframLibrary.h"
#include "WolframImageLibrary.h"
#include <vector>
#include "../Core/OptimizedGif.h"


extern "C"
{
static int counter = 0;
static std::vector<OptimizedGif*> gifs;

DLLEXPORT int gifInit(WolframLibraryData libData, mint Argc, MArgument *Args, MArgument Res)
{
    char* filename = MArgument_getUTF8String(Args[0]);
    int width = MArgument_getInteger(Args[1]);
    int height = MArgument_getInteger(Args[2]);
    int colors = MArgument_getInteger(Args[3]);

    if(width < 0 || height < 0 ||  colors < 0)
    {
        return LIBRARY_FUNCTION_ERROR;
    }

    OptimizedGif* optimizedGif = new OptimizedGif(filename, width, height, colors);
    gifs.push_back(optimizedGif);

    MArgument_setInteger(Res, counter++);
    return LIBRARY_NO_ERROR;
}

DLLEXPORT int gifAddFrame(WolframLibraryData libData, mint Argc, MArgument *Args, MArgument Res)
{
    int id = MArgument_getInteger(Args[0]);
    if(id < 0 || id >= counter)
        return LIBRARY_FUNCTION_ERROR;

    MImage input = MArgument_getMImage(Args[1]);

    unsigned char* data = libData->imageLibraryFunctions->MImage_getByteData(input);

    gifs[id]->addFrame(data);

    return LIBRARY_NO_ERROR;
}

DLLEXPORT int gifAddFrameCs(WolframLibraryData libData, mint Argc, MArgument *Args, MArgument Res)
{
    int id = MArgument_getInteger(Args[0]);
    MImage input = MArgument_getMImage(Args[1]);
    mint cs = MArgument_getInteger(Args[2]);

    if(id < 0 || id >= counter)
        return LIBRARY_FUNCTION_ERROR;

    if(cs < 0)
        return LIBRARY_FUNCTION_ERROR;
    

    unsigned char* data = libData->imageLibraryFunctions->MImage_getByteData(input);

    gifs[id]->addFrame(data, cs);

    return LIBRARY_NO_ERROR;
}

DLLEXPORT int gifClose(WolframLibraryData libData, mint Argc, MArgument *Args, MArgument Res)
{
    int id = MArgument_getInteger(Args[0]);
    if(id < 0 || id >= counter)
    {
        return LIBRARY_FUNCTION_ERROR;
    }

    if(gifs[id] != NULL)
    {
        gifs[id]->close();
        delete gifs[id];

        return LIBRARY_NO_ERROR;
    } 
    
    return LIBRARY_FUNCTION_ERROR;

}

}
