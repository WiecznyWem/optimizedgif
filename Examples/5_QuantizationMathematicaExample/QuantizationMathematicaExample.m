(* ::Package:: *)

<<OptimizedGif`
SetDirectory[NotebookDirectory[]];


width = 256;
height = 256;
colors = 15;
gif2 = gifInit["mathematica_dithered_15_colors.gif", width, height, colors];
For[i=1,i<=25,i++,
img = ColorQuantize[RemoveAlphaChannel[Image[
Plot3D[Sin[i/10*x+y]*Cos[i/10*y-x]/(i+x^2+y^2), {x, -Pi, Pi}, {y, -Pi, Pi}, Background->White, AspectRatio->1, PlotRange->{-0.5,0.5}], 
ColorSpace->"RGB", ImageSize->{width, height}]], colors, Dithering->True, Method->"Octree"];
gifAddFrameCs[gif2, img, 5];
];
gifClose[gif2];


width = 256;
height = 256;
colors = 15;
gif2 = gifInit["mathematica_nondithered_15_colors.gif", width, height, colors];
For[i=1,i<=25,i++,
img = ColorQuantize[RemoveAlphaChannel[Image[
Plot3D[Sin[i/10*x+y]*Cos[i/10*y-x]/(i+x^2+y^2), {x, -Pi, Pi}, {y, -Pi, Pi}, Background->White, AspectRatio->1, PlotRange->{-0.5,0.5}], 
ColorSpace->"RGB", ImageSize->{width, height}]], colors, Dithering->False, Method->"Octree"];
gifAddFrameCs[gif2, img, 5];
];
gifClose[gif2];


width = 256;
height = 256;
colors = 7;
gif2 = gifInit["mathematica_nondithered_7_colors.gif", width, height, colors];
For[i=1,i<=25,i++,
img = ColorQuantize[RemoveAlphaChannel[Image[
Plot3D[Sin[i/10*x+y]*Cos[i/10*y-x]/(i+x^2+y^2), {x, -Pi, Pi}, {y, -Pi, Pi}, Background->White, AspectRatio->1, PlotRange->{-0.5,0.5}], 
ColorSpace->"RGB", ImageSize->{width, height}]], colors, Dithering->False, Method->"Octree"];
gifAddFrameCs[gif2, img, 5];
];
gifClose[gif2];


width = 256;
height = 256;
colors = 15;
gif2 = gifInit["dithered_15_colors.gif", width, height, colors];
For[i=1,i<=25,i++,
img = RemoveAlphaChannel[Image[
Plot3D[Sin[i/10*x+y]*Cos[i/10*y-x]/(i+x^2+y^2), {x, -Pi, Pi}, {y, -Pi, Pi}, Background->White, AspectRatio->1, PlotRange->{-0.5,0.5}], 
ColorSpace->"RGB", ImageSize->{width, height}]];
gifAddFrameCs[gif2, img, 5];
];
gifClose[gif2];
