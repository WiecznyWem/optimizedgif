#include <iostream>
#include <cmath>
#include <vector>
#include "OptimizedGif.h"

int quantA(int diam = 64, double modi = 1.0, int frames = 12, const char* filename = "quantA.gif")
{
    const int width = diam;
    const int height = diam;
    const int size = width * height * 3;
    OptimizedGif gif(filename, width, height);

    for(int count = 0; count <= frames; count++)
    {
        double t = modi*(double)count / frames;
        std::vector<unsigned char> frame(size);
        for(int i = 0; i < size; i++)
        {
            int y = i / ( 3* width);
            int x = (i/3) % ( width);
            int k = i % 3;

            if(k == 0)
            {
                frame[i] = ((int)(x + t*y)) % 256;
            }
            if(k == 1)
            {
                frame[i] = ((int)(2*t*x + y)) % 256;
            }
            if(k == 2)
            {
                frame[i] = ((int)(x + 2*t*y)) % 256;
            }  

        }

        gif.addFrame(frame.data(), 10);
    }

    gif.close();

    return 0;
}

int main()
{
    quantA(256);
    quantA(256, 0.2, 120,  "quantB.gif");

    return 0;
}
