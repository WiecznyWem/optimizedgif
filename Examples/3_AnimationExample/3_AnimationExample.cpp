#include <iostream>
#include <cmath>
#include <vector>
#include "OptimizedGif.h"

int animA(int diam = 64, const char* filename = "animA.gif")
{
    const int width = diam;
    const int height = diam;
    const int size = width * height * 3;
    

    OptimizedGif gif(filename, width, height);

    unsigned int ms = 500;
    
    {
        std::vector<unsigned char> frame(size);
        for(int i = 0; i < size; i++)
        {
            int y = i / ( 3* width);
            int x = (i/3) % ( width);
            int k = i % 3;

            if(x == y)
            {
                frame[i] = 0xFF;
            }
            else
            {
                frame[i] = 0x00;
            }
        }
        gif.addFrame(frame.data(), 0);
    }
    {
        std::vector<unsigned char> frame(size);
        for(int i = 0; i < size; i++)
        {
            int y = i / ( 3* width);
            int x = (i/3) % ( width);
            int k = i % 3;

            if(x <= height - y)
            {
                frame[i] = 0xAA;
            }
            else
            {
                frame[i] = 0x00;
            }
        }
        gif.addFrame(frame.data(), ms / 10);
    }

    gif.close();

    return 0;
}


int animF(int diam = 64, const char* filename = "animF.gif")
{
    const int width = diam;
    const int height = diam;
    const int size = width * height * 3;
    OptimizedGif gif(filename, width, height);

    const int frames = 10;
    for(int count = 1; count <= frames; count++)
    {
        double t = (double)count / frames;
        std::vector<unsigned char> frame(size);
        for(int i = 0; i < size; i++)
        {
            int y = i / ( 3* width);
            int x = (i/3) % ( width);
            int k = i % 3;

            if(k == 0)
            {
                frame[i] = 32*(((int)(x/8 + t*y/16)) % 8);
            }
            if(k == 1)
            {
                frame[i] = 32*(((int)(x/16 + t*y/16)) % 8);
            }
            if(k == 2)
            {
                frame[i] = 32*(((int)(x/8 + t*y/16)) % 8);
            }  

        }

        gif.addFrame(frame.data(), 10);
    }

    gif.close();

    return 0;
}

int main()
{
    animA(1024);
    animF(256);

    return 0;
}
