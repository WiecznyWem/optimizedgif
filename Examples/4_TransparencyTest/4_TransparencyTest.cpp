#include <iostream>
#include <cmath>
#include <vector>
#include "OptimizedGif.h"

int transA(int diam = 64, const char* filename = "transA.gif")
{
    const int width = diam;
    const int height = diam;
    const int size = width * height * 3;
    

    OptimizedGif gif(filename, width, height, 3, false);

    std::vector<unsigned char> frame(size);
    for(int i = 0; i < size; i++)
    {
        int y = i / ( 3* width);
        int x = (i/3) % ( width);
        int k = i % 3;

        if(x <= y)
        {
            frame[i] = 0xFF;
        }
        else
        {
            frame[i] = 0x00;
        }
    }
    gif.addFrame(frame.data());

    gif.close();

    return 0;
}

int transB(int diam = 512, const char* filename = "transB.gif")
{
    const int width = diam;
    const int height = diam;
    const int size = width * height * 3;
    
    std::vector<unsigned char> frame(size);
    OptimizedGif gif(filename, width, height, 127, true);
    {
    
        for(int i = 0; i < size; i++)
        {
            int y = i / ( 3* width);
            int x = (i/3) % ( width);
            int k = i % 3;

            if(x <= y)
            {
                frame[i] = 0xFF;
            }
            else
            {
                frame[i] = 0x00;
            }
        }
        gif.addFrame(frame.data());
    }
    
    for(int nn = 1; nn < 5; nn++)
    {
        for(int i = 0; i < size; i++)
        {
            int y = i / ( 3* width);
            int x = (i/3) % ( width);
            int k = i % 3;

            if( x < nn * 50 || y < nn * 50)
                continue;

            if( x > width - nn * 50 || y > height - nn * 50)
                continue;

            if(x <= y)
            {
                frame[i] = 30 * nn;
            }
            else
            {
                frame[i] = 0;
            }
        }
        gif.addFrame(frame.data());
    }

    gif.close();

    return 0;
}

#define RED 0xFF, 0x00, 0x00
#define GREEN 0x00, 0xFF, 0x00
int transC()
{
    const int width = 9;
    const int height = 2;
    const int size = width * height * 3;
    OptimizedGif gif("transC.gif", width, height, 15, true);

    {
        unsigned char frame[size] = {GREEN, RED, GREEN, GREEN, RED, RED, GREEN, GREEN, RED, RED, GREEN, RED, RED, GREEN, GREEN, GREEN, GREEN, GREEN};
        gif.addFrame(frame);
    }
    {
        unsigned char frame[size] = {GREEN, RED, RED,     RED, RED, RED, GREEN, GREEN, RED, RED,   RED, RED, RED, GREEN, GREEN, GREEN, GREEN, GREEN};
        gif.addFrame(frame);
    }

    gif.close();

    // expected output on second frame
    // RED, RED, RED
    // RED, RED, RED
    return 0;
}

int transD()
{
    const int width = 5;
    const int height = 2;
    const int size = width * height * 3;
    OptimizedGif gif("transD.gif", width, height, 15, true);

    {
        unsigned char frame[size] = {GREEN, RED, RED, GREEN, RED, RED, GREEN, GREEN, RED, RED};
        gif.addFrame(frame);
    }
    {
        unsigned char frame[size] = {GREEN, RED, RED, RED,   RED, RED, GREEN, GREEN, RED, RED};
        gif.addFrame(frame);
    }

    gif.close();

    // expected output on second frame
    // RED
    return 0;
}

int main()
{
    std::cout<<"Test: 4_TransparencyTest"<<std::endl;
    transA(1024);
    transB();
    transC();
    transD();
    return 0;
}
