(* Run from console by `math -script MathematicaAnimation.m` *)

<<OptimizedGif`

width = 987;
height = 610;
colors = 233;
gif = gifInit["animation.gif", width, height, colors];

For[i = 1,i < 25,i++,
	img = RemoveAlphaChannel[
		Image[
			Plot3D[
				Sin[i/10*x+y]*Cos[i/10*y-x]/(i+x^2+y^2), 
				{x, -Pi, Pi}, {y, -Pi, Pi}, 
				Background->White, 
				AspectRatio->1, 
				PlotRange->{-0.5,0.5}
			], 
			ColorSpace->"RGB", 
			ImageSize->{width, height}
		]
	];
	gifAddFrameCs[gif, img, 5];
];
gifClose[gif];


