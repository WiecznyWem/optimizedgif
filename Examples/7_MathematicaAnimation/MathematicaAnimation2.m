(* Run from console by `math -script MathematicaAnimation.m` *)

<<OptimizedGif`

width = 512;
height = 512;
colors = 31;
gif = gifInit["animation.gif", width, height, colors];

For[i = 1,i < 25,i++,
	p = Abs[i-12]  + 5;
	img = RemoveAlphaChannel[
		Image[
			Plot3D[
				Sin[p/10*x+y]*Cos[p/10*y-x]/(p+x^2+y^2), 
				{x, -Pi, Pi}, {y, -Pi, Pi}, 
				Background->White, 
				AspectRatio->1, 
				PlotRange->{-0.5,0.5}
			], 
			ColorSpace->"RGB", 
			ImageSize->{width, height}
		]
	];
	gifAddFrameCs[gif, img, 5];
];
gifClose[gif];


