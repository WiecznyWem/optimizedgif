#include <iostream>
#include <cmath>
#include <vector>
#include "OptimizedGif.h"

int imageA(int diam = 64, const char* filename = "imageA.gif")
{
    const int width = diam;
    const int height = diam;
    const int size = width * height * 3;
    std::vector<unsigned char> frame(size);
    for(int i = 0; i < size; i++)
    {
        int y = i / ( 3* width);
        int x = (i/3) % ( width);
        int k = i % 3;

        if(x == y)
        {
            frame[i] = 0xFF;
        }
        else
        {
            frame[i] = 0x00;
        }
            
    }

    OptimizedGif gif(filename, width, height);
    gif.addFrame(frame.data());
    gif.close();

    return 0;
}

int imageB()
{
    const int width = 2;
    const int height = 2;
    const int size = width * height * 3;
    unsigned char frame[size];

    frame[0] = 0x55;
    frame[1] = 0x55;
    frame[2] = 0x55;

    frame[3] = 0xFF;
    frame[4] = 0x00;
    frame[5] = 0x00;

    frame[6] = 0xFF;
    frame[7] = 0xFF;
    frame[8] = 0xFF;

    frame[9] = 0x00;
    frame[10] = 0x00;
    frame[11] = 0x00;

    OptimizedGif gif("imageB.gif", width, height);
    gif.addFrame(frame);
    gif.close();

    return 0;
}

int imageC()
{
    const int width = 20;
    const int height = 2;
    const int size = width * height * 3;
    unsigned char frame[size];

    for(int i = 0; i < 10; i++)
    {
        frame[12*i+0] = 0x55;
        frame[12*i+1] = 0x55;
        frame[12*i+2] = 0x55;

        frame[12*i+3] = 0xFF;
        frame[12*i+4] = 0x00;
        frame[12*i+5] = 0x00;

        frame[12*i+6] = 0xFF;
        frame[12*i+7] = 0xFF;
        frame[12*i+8] = 0xFF;

        frame[12*i+9] = 0x00;
        frame[12*i+10] = 0x00;
        frame[12*i+11] = 0x00;
    }

    OptimizedGif gif("imageC.gif", width, height);
    gif.addFrame(frame);
    gif.close();

    return 0;
}

int imageE()
{
    const int width = 32;
    const int height = 32;
    const int size = width * height * 3;
    unsigned char frame[size];

    for(int i = 0; i < width * height; i++)
    {
        frame[3*i+0] = 16 * (1+ (i % 16)) - 1;
        frame[3*i+1] = 16 * (1+ (i % 16)) - 1;
        frame[3*i+2] = 16 * (1+ (i % 16)) - 1;

    }

    OptimizedGif gif("imageE.gif", width, height);
    gif.addFrame(frame);
    gif.close();

    return 0;
}

int imageF(int diam = 64, const char* filename = "imageF.gif")
{
    const int width = diam;
    const int height = diam;
    const int size = width * height * 3;
    std::vector<unsigned char> frame(size);
    for(int i = 0; i < size; i++)
    {
        int y = i / ( 3* width);
        int x = (i/3) % ( width);
        int k = i % 3;

        if(k == 0)
        {
            frame[i] = (x + y) % 256;
        }
        if(k == 1)
        {
            frame[i] = (2*x + y) % 256;
        }
        if(k == 2)
        {
            frame[i] = (x + 2*y) % 256;
        }  

    }

    OptimizedGif gif(filename, width, height);
    gif.addFrame(frame.data());
    gif.close();

    return 0;
}

int main()
{
    imageA();
    imageB();
    imageC();
    imageA(1024, "imageD.gif"); 
    imageE();
    imageF(2048);
}
