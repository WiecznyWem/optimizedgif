<<OptimizedGif`
SetDirectory[NotebookDirectory[]];


img = RemoveAlphaChannel@Image[Plot[Sin[x], {x, -Pi, Pi}, Background->White], ColorSpace->"RGB"];
{width, height} = ImageDimensions[img];
gif1 = gifInit["simple_animation_1.gif", width, height, 255];
Print[gif1];
gifAddFrame[gif1, img];
gifClose[gif1];


width = 256;
height = 256;
colors = 3;
gif = gifInit["simple_animation_2.gif", width, height, colors];

faces = PolyhedronData["Dodecahedron", "Faces"];
frames = 100;

Monitor[For[i = 0, i < frames, i++,
   verticles = 
    Transpose[
     EulerMatrix[{-((2 Pi i)/frames), (2 Pi i)/frames, 0}].Transpose@
       PolyhedronData["Dodecahedron", "Vertices"]];
   
   img = RemoveAlphaChannel[Image[
      Graphics3D[{Thick,  GraphicsComplex[verticles, Line[faces]]}, 
       Boxed -> False, ViewPoint -> {0, 0, Infinity},  
       PlotRange -> {{-2, 2}, {-2, 2}, {-2, 2}}],
      ColorSpace -> "RGB", ImageSize -> {width, height}]];
   
   gifAddFrameCs[gif, img, 6];
   
   ], i];

gifClose[gif];
