#include <iostream>
#include <cmath>
#include <vector>
#include "OptimizedGif.h"
#include "GifEncoding/Dictionary.h"
#include "GifEncoding/CrossByteBuilder.h"
#include "GifEncoding/LzwEncoder.h"


int dictCodeSizeTest()
{
    int initialSize = 5;
    int targetSize = 9;
    Dictionary dict(initialSize);
    unsigned short item = 3;

    int toAdd = pow(2, initialSize) - 1 - dict.getEOI() ;
    for(int i = 0; i < toAdd; i++)
    {
        item = dict.insert(item, 3);
        if(dict.getCodeSize() != initialSize)
        {
            return dict.getNextCode(); 
        }
    }

    for(int j = initialSize+1; j < targetSize; j++)
    {
        toAdd = pow(2, j-1);
        for(int i = 0; i < toAdd; i++)
        {
            item = dict.insert(item, 3);
            if(dict.getCodeSize() != j)
            {
                return dict.getNextCode(); 
            }
        }
 
    }

    return 0;
}

int dictEncodingTest()
{
    const int n = 30;
    byte data[n] = {1,6,1,8,0,3,3,9,8,8,
                       1,6,1,8,1,6,1,8,0,3,
                       1,6,1,8,0,3,3,9,8,8};

    const int m = 14;
    byte properOutput[m] = {0x30, 0x98, 0x80, 0xC0, 0x18, 0x09, 0x21, 0x49, 
                            0x79, 0x1D, 0x9E, 0x85, 0x69, 0x11};

    std::vector<byte> output = LzwEncoder::encode(data, n, 5);
    
    if(output.size() != m)
        return 1;

    for(int i = 0; i < m; i++)
    {
        if(output[i] != properOutput[i])
            return i+2;
    }
    
    return 0;
}

int crossByteBuilderTest()
{
    const int n = 20;
    unsigned short input[n] = {1,6,1,8,0,3,3,9,8,8,
                             18,20,28,21,3,30,22,24,26,7};
    byte codeSize[n] = {3,3,3,4,4,4,4,4,4,4,
                        5,5,5,6,6,8,8,12,12,3};
    const int m = 14;
    byte properOutput[m] = {0x71, 0x10, 0x66, 0x12, 0x51, 0x52, 0x5E, 0x0D,
                            0x1E, 0x16, 0x18, 0xA0, 0x01, 0x07};

    CrossByteBuilder builder(n);
    for(int i = 0; i < n; i++)
    {
        builder.push_back(input[i], codeSize[i]);
    }
    std::vector<byte> output = builder.getData();

    if(m != output.size())
        return m+1;

    for(int i = 0; i < output.size(); i++)
        if(output[i] != properOutput[i])
        {
            return i+1;
        }
            

    return 0;
}

int smallTest()
{
    std::vector<byte> vec = {2,1,3,0};
    std::vector<byte> output = LzwEncoder::encode(vec.data(), vec.size(), 3);
    std::vector<byte> properOutput = {0x54, 0x06, 0x05};

    if(properOutput.size() != output.size())
        return properOutput.size()+1;

    for(int i = 0; i < properOutput.size(); i++)
        if(output[i] != properOutput[i])
            return i+1;

    return 0;
}

int main()
{
    int ret =  0;
    
    ret = dictCodeSizeTest();
    std::cout<<"dictCodeSizeTest return code: "<<ret<<std::endl;
    ret = dictEncodingTest();
    std::cout<<"dictEncodingTest return code: "<<ret<<std::endl;
    ret = crossByteBuilderTest();
    std::cout<<"crossByteBuilderTest return code: "<<ret<<std::endl;
    ret = smallTest();
    std::cout<<"smallTest return code: "<<ret<<std::endl;

    return 0;
}
