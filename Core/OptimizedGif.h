#ifndef OPTIMIZED_GIF_H
#define OPTIMIZED_GIF_H

#include <string>

#if defined(_MSC_VER)
    #define EXPORT __declspec(dllexport)
#elif defined(__GNUC__)
    #define EXPORT __attribute__((visibility("default")))
#else
    #define EXPORT
    #pragma warning Unknown dynamic link export semantics.
#endif

class GifEncoder;
class FrameCollector;

class EXPORT OptimizedGif
{
public:
    OptimizedGif(   const std::string filename, 
                    const int width, 
                    const int height, 
                    unsigned short colors = 127, 
                    bool looped = true); 
    ~OptimizedGif();
    void addFrame(unsigned char* frame, unsigned int cs = 10);
    void close();

private: 
    const int width;
    const int height;
    unsigned short colors;

    GifEncoder* ge;
    FrameCollector* fc;
};

#endif
