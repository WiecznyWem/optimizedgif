#ifndef COMMON_H
#define COMMON_H

#include <tuple>

typedef unsigned char byte;

#define MAX_COLORS 256
#define DEFAULT_PERSISTENCE_THRESHOLD 3

struct RGBColor
{
    byte r, g, b;
};

double RGBDistance(const RGBColor& a, const RGBColor& b);

bool operator <(const RGBColor& rhs, const RGBColor& lhs);
bool operator ==(const RGBColor& rhs, const RGBColor& lhs);
bool operator !=(const RGBColor& rhs, const RGBColor& lhs);

const RGBColor TRANSPARENT = {129,126,127};

void informAboutStatus(int ret, const char* function, const char* file, int line);

#define trace(ret) informAboutStatus(ret, __FUNCTION__, __FILE__, __LINE__)

#endif
