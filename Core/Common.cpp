#include "Common.h"
#include <iostream>

double RGBDistance(const RGBColor& a, const RGBColor& b)
{
    return (a.r - b.r) * (a.r - b.r) + (a.g - b.g) * (a.g - b.g) + (a.b - b.b) * (a.b - b.b);
}

bool operator <(const RGBColor& rhs, const RGBColor& lhs) {
    return std::tie(rhs.r, rhs.g, rhs.b) < std::tie(lhs.r, lhs.g, lhs.b);
}

bool operator ==(const RGBColor& rhs, const RGBColor& lhs) {
    return std::tie(rhs.r, rhs.g, rhs.b) == std::tie(lhs.r, lhs.g, lhs.b);
}

bool operator !=(const RGBColor& rhs, const RGBColor& lhs) {
    return !(lhs == rhs);
}

void informAboutStatus(int ret, const char* function, const char* file, int line)
{
    if(ret != 0)
        std::cout<<"RETURNED "<<ret<<" AT "<<file<<":"<<line<<" IN "<<function<<std::endl;
}
