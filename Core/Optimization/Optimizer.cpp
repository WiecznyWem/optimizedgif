#include "Optimizer.h"

Optimizer::Optimizer(const unsigned short width, const unsigned short height, 
const byte* quantizedFrame, const std::vector<RGBColor>& colorTable, FrameCollector* fc) 
    : width(width), height(height), quantizedFrame(quantizedFrame), 
    colorTable(colorTable), fc(fc), offsetLeft(0), offsetTop(0), 
    outputWidth(0), outputHeight(0)
{


}

void Optimizer::optimize()
{  
    crop();
    output = std::vector<byte>(outputWidth*outputHeight);
    differentitate();
    lzwOptimize();
}

void Optimizer::crop()
{
    outputWidth = width;
    outputHeight = height;

    bool topFound = false;
    for(unsigned short top = 0; top < height; top++)
    {
        for(unsigned short left = 0; left < width; left++)
        {
            unsigned int i = width * top + left;

            byte Acode = (*fc->getFrame(0))[i];
            RGBColor Acolor = (*fc->getTable(0))[Acode];

            if(colorTable[quantizedFrame[i]] != Acolor)
            {
                topFound = true;
                offsetTop = top;
                break;
            }
        }
        if(topFound)
            break;
    }

    bool leftFound = false;
    for(unsigned short left = 0; left < width; left++)
    {
        for(unsigned short top = 0; top < height; top++)
        {
            unsigned  int i = width * top + left;
            if(colorTable[quantizedFrame[i]] != (*fc->getTable(0))[(*fc->getFrame(0))[i]])
            {
                leftFound = true;
                offsetLeft = left;
                break;
            }
        }
        if(leftFound)
            break;
    }

    bool rightFound = false;
    for(int right = width - 1; right >= offsetLeft; right--)
    {
        for(unsigned short top = offsetTop; top < height; top++)
        {
            unsigned  int i = width * top + right;
            if(colorTable[quantizedFrame[i]] != (*fc->getTable(0))[(*fc->getFrame(0))[i]])
            {
                rightFound = true;
                outputWidth = right - offsetLeft + 1;
                break;
            }
        }
        if(rightFound)
            break;
    }

    bool bottomFound = false;
    for(int bottom = height - 1; bottom >= offsetTop; bottom--)
    {
        for(int right = width - 1; right >= offsetLeft; right--)
        {
            unsigned  int i = width * bottom + right;
            if(colorTable[quantizedFrame[i]] != (*fc->getTable(0))[(*fc->getFrame(0))[i]])
            {
                bottomFound = true;
                outputHeight = bottom - offsetTop + 1;
                break;
            }
        }
        if(bottomFound)
            break;
    }
}

void Optimizer::differentitate()
{
    unsigned int iter = 0;
    for(unsigned int top = offsetTop; top < offsetTop + outputHeight; top++)
    {
        for(unsigned int left = offsetLeft; left < offsetLeft + outputWidth; left++)
        {
            unsigned int i = width * top + left;
            
            if(colorTable[quantizedFrame[i]] == (*fc->getTable(0))[(*fc->getFrame(0))[i]])
            {
                output[iter] = 0;
            } else 
            {
                output[iter] = quantizedFrame[i];
            }

            ++iter;
        }
    }
}

void Optimizer::lzwOptimize()
{
    bool firstInGroup = true;

    for(unsigned short y = 0; y < outputHeight; y++)
    {
        unsigned i2 = (y-1) * outputWidth + (outputWidth-1);
        for(unsigned short x = 0; x < outputWidth; x++)
        {
            unsigned int i = y * outputWidth + x;
            unsigned int j = (y + offsetTop) * width + (offsetLeft + x);

            if(firstInGroup && output[i] == quantizedFrame[j])
            {
                firstInGroup = false;

                bool backwaywrap = false;
                bool endOfGroup = false;
                for(int yy = (x>0 ? y : y-1); yy >= 0; yy--)
                {
                    unsigned ii2 = (yy+1) * outputWidth + 0;
                    for(int xx = (backwaywrap ? outputWidth-1 : x-1 ); xx >= 0; xx--)
                    {
                        unsigned int ii = yy * outputWidth + xx;
                        unsigned int jj = (yy + offsetTop) * width + (offsetLeft + xx);
                        if( (xx < outputWidth - 1 && output[ii+1] == quantizedFrame[jj])
                            ||
                                (yy < outputHeight - 1 && output[ii2] == quantizedFrame[jj]) )
                        {
                            output[ii] = quantizedFrame[jj];
                        } else
                        {
                            endOfGroup = true;
                            break;
                        }
                            
                    }
                    backwaywrap = true;
                    if(endOfGroup)
                        break;
                }

            } 

            if( (x > 0 && output[i-1] == quantizedFrame[j])
            ||
                (y > 0 && output[i2] == quantizedFrame[j]) )
            {
                output[i] = quantizedFrame[j];
            } else
                firstInGroup = true;
        }
    }
}
