#include "FrameCollector.h"

FrameCollector::FrameCollector(int maxSize) 
    : maxSize(maxSize), frames(maxSize), tables(maxSize), counters(maxSize), current(0)
{
}

void FrameCollector::push(std::vector<byte>& frame, std::vector<RGBColor>& table, std::vector<unsigned int>& counter)
{
    frames[(current + 1 ) % maxSize] = std::move(frame);
    tables[(current + 1 ) % maxSize] = std::move(table);
    counters[(current + 1 ) % maxSize] = std::move(counter);
    ++current;
}

const std::vector<byte>* FrameCollector::getFrame(int last) const
{
    if(last < 0 || last >= getSize())
        return NULL;

    return &(frames[(maxSize + current - last) % maxSize]);
}

const std::vector<RGBColor>* FrameCollector::getTable(int last) const
{
    if(last < 0 || last >= getSize())
        return NULL;

    return &(tables[(maxSize + current - last) % maxSize]);
}

const std::vector<unsigned int>* FrameCollector::getCounter(int last) const
{
    if(last < 0 || last >= getSize())
        return NULL;

    return &(counters[(maxSize + current - last) % maxSize]);
}

int FrameCollector::getSize() const
{
    if(current < maxSize) 
        return current;

    return maxSize;
}
