#ifndef TRANSPARENCY_OPTIMIZER_H
#define TRANSPARENCY_OPTIMIZER_H

#include "../Common.h"
#include "FrameCollector.h"
#include <vector>

class Optimizer
{
public:
    Optimizer(const unsigned short width, const unsigned short height, const byte* quantizedFrame, const std::vector<RGBColor>& colorTable, FrameCollector* fc);
    void optimize(); 
    
    std::vector<byte> output;
    unsigned short offsetLeft;
    unsigned short offsetTop;
    unsigned short outputWidth;
    unsigned short outputHeight;
private:
    const unsigned short width;
    const unsigned short height; 
    const byte* quantizedFrame;
    const std::vector<RGBColor>& colorTable;
    FrameCollector* fc;
    
    void crop(); 
    void differentitate(); 
    void lzwOptimize(); 
};

#endif
