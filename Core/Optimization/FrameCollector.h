#ifndef FRAME_COLLECTOR_H
#define FRAME_COLLECTOR_H

#include "../Common.h"
#include <vector>

class FrameCollector
{
public:
    FrameCollector(int maxSize = 5);
    
    void push(std::vector<byte>& frame, std::vector<RGBColor>& table, std::vector<unsigned int>& counter);
    const std::vector<byte>* getFrame(int last) const;
    const std::vector<RGBColor>* getTable(int last) const;
    const std::vector<unsigned int>* getCounter(int last) const;
    int getSize() const;

private:
    std::vector< std::vector<byte> > frames;
    std::vector< std::vector<RGBColor> > tables;
    std::vector< std::vector<unsigned int> > counters;
	int maxSize;
    int current;
};

#endif
