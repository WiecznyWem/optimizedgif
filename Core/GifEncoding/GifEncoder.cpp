#include "GifEncoder.h"

GifEncoder::GifEncoder( const std::string filename, 
                        const int width, 
                        const int height, 
                        unsigned short colors, 
                        const bool looped)
    : width(width), height(height), looped(looped)
{
    file.open(filename.c_str(), std::ios::binary);

    colors += 1;
    if(colors > 256)
        colors = 256;
    this->colors = colors;

    colorBits = 0;
    unsigned short c = colors - 1;
    while(c > 0)
    {
        c >>= 1;
        ++colorBits; 
    }

    Header header;
    header.write(file);
     
    LogicalScreenDescriptor logicalScreenDescriptorSection;
    logicalScreenDescriptorSection.logicalScreenWidth = width;
    logicalScreenDescriptorSection.logicalScreenHeight = height;
    logicalScreenDescriptorSection.colorResolution = colorBits - 1;
    logicalScreenDescriptorSection.write(file);

    if(looped)
    {
        NetscapeLoopingApplicationExtension netscapeLoopingApplicationExtensionSection;
        netscapeLoopingApplicationExtensionSection.write(file);
    }

    GraphicControlExtension graphicControlExtensionSection;
    graphicControlExtensionSection.disposalMethod = 0;
    graphicControlExtensionSection.userInputFlag = true;
    graphicControlExtensionSection.transparentColorFlag = true;
    graphicControlExtensionSection.transparentColorIndex = 0;
    graphicControlExtensionSection.delayTime = 0;
    graphicControlExtensionSection.write(file);
}

void GifEncoder::addFrame(  const std::vector<RGBColor>& colorTable, 
                            byte* frame, 
                            const unsigned short width, 
                            const unsigned short height, 
                            const unsigned short leftOffset, 
                            const unsigned short topOffset, 
                            const unsigned short delay)
{
    int initialCodeSize = colorBits + 1;
    std::vector<byte> output = LzwEncoder::encode(frame, width*height, initialCodeSize);

    ImageDescriptor imageDescriptorSection;
    ColorTable colorTableSection;
    RasterData rasterDataSection;
    GraphicControlExtension graphicControlExtensionSection;

    imageDescriptorSection.imageLeftPosition = leftOffset;
    imageDescriptorSection.imageTopPosition = topOffset;
    imageDescriptorSection.imageWidth = width;
    imageDescriptorSection.imageHeight = height;
    imageDescriptorSection.localColorTableFlag = true;
    imageDescriptorSection.sizeOfLocalColorTable = colorBits-1;
    colorTableSection.size = colorTable.size();
    colorTableSection.data = colorTable.data();
    rasterDataSection.lzwCodeSize = initialCodeSize;
    rasterDataSection.encodedData = output.data();
    rasterDataSection.encodedLength = output.size();
    graphicControlExtensionSection.disposalMethod = 0;
    graphicControlExtensionSection.userInputFlag = true;
    graphicControlExtensionSection.transparentColorFlag = true;
    graphicControlExtensionSection.transparentColorIndex = 0;
    graphicControlExtensionSection.delayTime = delay;

    graphicControlExtensionSection.write(file);
    imageDescriptorSection.write(file);
    colorTableSection.write(file);
    rasterDataSection.write(file);
}

void GifEncoder::close()
{
    Trailer trailer;
    trailer.write(file);

    file.close();
}
