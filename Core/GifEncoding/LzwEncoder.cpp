#include "LzwEncoder.h"

std::vector<byte> LzwEncoder::encode(const byte* data, int dataSize, byte minimumCodeSize)
{
    Dictionary dict(minimumCodeSize);
    CrossByteBuilder builder(dataSize);

    builder.push_back(dict.getCC(), dict.getCodeSize());

    unsigned short item = data[0];
    for(int i = 1; i < dataSize; i++)
    {
        if( dict.getNextCode() == dict.getCC())
        {
            builder.push_back(dict.getCC(), dict.getCodeSize());
            dict.reset();
        }

        unsigned short found = dict.find(item, data[i]); 
        if(found == dict.getEOI())
        {
            int size = dict.getCodeSize();
            dict.insert(item, data[i]);
            builder.push_back(item, size);
            item = data[i];
        } else
            item = found;
    }

    builder.push_back(item, dict.getCodeSize());
    builder.push_back(dict.getEOI(), dict.getCodeSize());

    return builder.getData();
}