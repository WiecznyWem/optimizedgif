#ifndef DICTIONARY_H
#define DICTIONARY_H

#include "../Common.h"
#include <vector>
#include <algorithm>

class Dictionary
{
public:

public:
    Dictionary(unsigned short initialCodeSize);
    unsigned short insert(unsigned short parent, byte value);
    unsigned short find(unsigned short parent, byte value);
    unsigned short getCC() const;
    unsigned short getEOI() const;
    unsigned short getNextCode() const;
    unsigned short getCodeSize() const;
    void reset();
    
private:
    const unsigned short maxCodeSize = 12;
    const unsigned short initialCodeSize;
    const unsigned short cc;
    const unsigned short eoiCode;    
    unsigned short nextCode;
    unsigned short codeSize;
    std::vector<unsigned short> memory;
    unsigned short incrementNextCode();
	const unsigned int maxValue = 256;
	const unsigned int memorySize = 4096 * maxValue;
};

#endif 
