#include "RasterData.h"

RasterData::RasterData()
    : lzwCodeSize(0), encodedLength(), encodedData(NULL)
{
    
}


int RasterData::write(std::ofstream &file)
{
    writeInitiator(file);
    writeSubBlocks(file);
    writeTerminator(file);

    return 0;
}

void RasterData::writeInitiator(std::ofstream &file)
{
    byte data[1];
    data[0] = lzwCodeSize - 1; 
    file.write(reinterpret_cast<char*>(data), 1*sizeof(byte));
}

void RasterData::writeSubBlocks(std::ofstream &file)
{
    for(unsigned long i = 0; i < encodedLength; i += 255)
    {
        byte data[256];

        unsigned long j = 1;
        for(unsigned long k = i; k < std::min(encodedLength, i + 255); k++)
        {
            data[j] = encodedData[k];
            ++j;
        }
        
        data[0] = j-1;

        file.write(reinterpret_cast<char*>(data), j*sizeof(byte));
        file.flush();
    }
}

void RasterData::writeTerminator(std::ofstream &file)
{
    byte data[1];
    data[0] =  0x00; 
    file.write(reinterpret_cast<char*>(data), 1*sizeof(byte));
}