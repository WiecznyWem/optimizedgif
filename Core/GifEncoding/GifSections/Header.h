#ifndef HEADER_H
#define HEADER_H

#include "Section.h"

class Header : public Section
{
public:
    int write(std::ofstream &file);
};

#endif