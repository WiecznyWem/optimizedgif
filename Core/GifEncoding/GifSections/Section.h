#ifndef SECTION_H
#define SECTION_H

#include <fstream>

class Section
{
public:
    virtual int write(std::ofstream &file) = 0;
};

#endif