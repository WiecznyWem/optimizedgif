#ifndef COLOR_TABLE_H
#define COLOR_TABLE_H

#include "../../Common.h"
#include "Section.h"


class ColorTable: public Section
{
public:
    ColorTable();

    unsigned short size;
    const RGBColor* data;

    int write(std::ofstream& file);
};

#endif