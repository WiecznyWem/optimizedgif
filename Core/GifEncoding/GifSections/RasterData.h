#ifndef RASTER_DATA
#define RASTER_DATA

#include "../../Common.h"
#include "Section.h"
#include <vector>
#include <algorithm>

class RasterData : public Section
{
public:
    RasterData();

    byte lzwCodeSize;
    unsigned long encodedLength;
    byte* encodedData;

    int write(std::ofstream &file);

private:
    void writeInitiator(std::ofstream &file);
    void writeSubBlocks(std::ofstream &file);
    void writeTerminator(std::ofstream &file);
};

#endif