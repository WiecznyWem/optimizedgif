#ifndef LOGICAL_SCREEN_DESCRIPTOR
#define LOGICAL_SCREEN_DESCRIPTOR

#include "../../Common.h"
#include "Section.h"

class LogicalScreenDescriptor : public Section
{
public:
    LogicalScreenDescriptor();

    unsigned short logicalScreenWidth;
    unsigned short logicalScreenHeight;
    bool globalColorTableFlag;
    byte colorResolution;
    bool sortFlag;
    byte sizeOfGlobalColorTable;
    byte backgroundColorIndex;
    byte pixelAspectRatio;

    int write(std::ofstream &file);
};

#endif