#ifndef NETSCAPE_LOOPING_APPLICATION_EXTENSION_H
#define NETSCAPE_LOOPING_APPLICATION_EXTENSION_H

#include "../../Common.h"
#include "Section.h"

class NetscapeLoopingApplicationExtension : public Section
{
public:
    NetscapeLoopingApplicationExtension();

    unsigned short loopCount;

    int write(std::ofstream &file);
};

#endif