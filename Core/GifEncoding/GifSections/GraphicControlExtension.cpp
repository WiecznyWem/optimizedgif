#include "GraphicControlExtension.h"

GraphicControlExtension::GraphicControlExtension()
    :     disposalMethod(0), userInputFlag(false), transparentColorFlag(false),
    delayTime(0), transparentColorIndex(0)
{

}

int GraphicControlExtension::write(std::ofstream &file)
{
    if(disposalMethod > 3)
        return 1337;
    
    byte data[8];
    data[0] = 0x21;
    data[1] = 0xF9;
    data[2] = 0x04;
    data[3] = 0; 
    data[3] |= (disposalMethod << 2);
    data[3] |= (userInputFlag << 1);
    data[3] |= transparentColorFlag;
    data[4] = delayTime % 256;
    data[5] = (delayTime>>8) % 256;
    if(!transparentColorFlag)
        transparentColorIndex = 0;
    data[6] = transparentColorIndex;
    data[7] = 0x00;

    file.write(reinterpret_cast<char*>(data), 8*sizeof(byte));

    return 0;
}