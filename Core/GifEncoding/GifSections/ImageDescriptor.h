#ifndef IMAGE_DESCRIPTOR_H
#define IMAGE_DESCRIPTOR_H

#include "../../Common.h"
#include "Section.h"

class ImageDescriptor: public Section
{
public:
    ImageDescriptor();

    unsigned short imageLeftPosition;
    unsigned short imageTopPosition;
    unsigned short imageWidth;
    unsigned short imageHeight;
    bool localColorTableFlag;
    bool interlaceFlag;
    bool sortFlag;
    byte sizeOfLocalColorTable;

    int write(std::ofstream& file);
};

#endif