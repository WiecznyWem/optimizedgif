#ifndef TRAILER_H
#define TRAILER_H

#include "../../Common.h"
#include "Section.h"

class Trailer : public Section
{
public:
    int write(std::ofstream &file);
};

#endif