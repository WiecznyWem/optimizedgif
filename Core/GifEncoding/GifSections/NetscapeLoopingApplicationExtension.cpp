#include "NetscapeLoopingApplicationExtension.h"

NetscapeLoopingApplicationExtension::NetscapeLoopingApplicationExtension()
    : loopCount(0)
{
}

int NetscapeLoopingApplicationExtension::write(std::ofstream &file)
{
    {
        byte data[3];
        data[0] = 0x21;
        data[1] = 0xFF;
        data[2] = 0x0B;
        file.write(reinterpret_cast<char*>(data), 3*sizeof(byte));
    }

    file.write("NETSCAPE2.0", 11); 

    {
        byte data[5];
        data[0] = 0x03;
        data[1] = 0x01;
        data[2] = loopCount % 256;
        data[3] = (loopCount>>8) % 256;
        data[4] = 0x00;
        file.write(reinterpret_cast<char*>(data), 5*sizeof(byte));
    }

    return 0;
}