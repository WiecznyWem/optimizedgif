#include "ImageDescriptor.h"

ImageDescriptor::ImageDescriptor()
    :     imageLeftPosition(0), imageTopPosition(0), imageWidth(0),
    imageHeight(0), localColorTableFlag(0), interlaceFlag(0),
    sortFlag(0), sizeOfLocalColorTable(0)
{

}

int ImageDescriptor::write(std::ofstream &file)
{
    if(sizeOfLocalColorTable > 7)
        return 1337;

    byte data[10];
    data[0] = 0x2C;
    data[1] = imageLeftPosition % 256;
    data[2] = (imageLeftPosition>>8) % 256;
    data[3] = imageTopPosition % 256;
    data[4] = (imageTopPosition>>8) % 256;
    data[5] = imageWidth % 256;
    data[6] = (imageWidth>>8) % 256;
    data[7] = imageHeight % 256;
    data[8] = (imageHeight>>8) % 256;
    data[9] = 0;
    data[9] |= (localColorTableFlag<<7);
    data[9] |= (interlaceFlag<<6);
    data[9] |= (sortFlag<<5);
    data[9] |= sizeOfLocalColorTable;

    file.write(reinterpret_cast<char*>(data), 10*sizeof(byte));

    return 0;
}