#include "ColorTable.h"

ColorTable::ColorTable()
: size(0), data(NULL)
{

}

int ColorTable::write(std::ofstream &file)
{
    for(unsigned short i = 0; i < size; i++)
    {
        file.write(reinterpret_cast<const char*>(&data[i]), sizeof(RGBColor));
    }

    return 0;
}