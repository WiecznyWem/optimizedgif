#include "Trailer.h"

int Trailer::write(std::ofstream &file)
{
    byte data[1];
    data[0] =  0x3B; 
    file.write(reinterpret_cast<char*>(data), 1*sizeof(byte));

    return 0;
}