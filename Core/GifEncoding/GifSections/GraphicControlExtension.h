#ifndef GRAPHIC_CONTROL_EXTENSION
#define GRAPHIC_CONTROL_EXTENSION

#include "../../Common.h"
#include "Section.h"

class GraphicControlExtension
{
public:
    GraphicControlExtension();

    byte disposalMethod;
    bool userInputFlag;
    bool transparentColorFlag;
    unsigned short delayTime;
    byte transparentColorIndex;

    int write(std::ofstream& file);
};

#endif