#include "LogicalScreenDescriptor.h"

LogicalScreenDescriptor::LogicalScreenDescriptor() 
    : logicalScreenWidth(0), 
    logicalScreenHeight(0), 
    globalColorTableFlag(0),
    colorResolution(0), 
    sortFlag(0), 
    sizeOfGlobalColorTable(0), 
    backgroundColorIndex(0), 
    pixelAspectRatio(0)
{
}

int LogicalScreenDescriptor::write(std::ofstream &file)
{
    if(colorResolution > 7)
        return 1337;

    if(sizeOfGlobalColorTable > 7)
        return 1338;

    byte data[7];
    data[0] = logicalScreenWidth % 256;
    data[1] = (logicalScreenWidth>>8) % 256;
    data[2] = logicalScreenHeight % 256;
    data[3] = (logicalScreenHeight>>8) % 256;
    data[4] = 0x0;
    data[4] |= (globalColorTableFlag<<7);
    data[4] |= (colorResolution<<4);
    data[4] |= (sortFlag<<3);
    data[4] |= sizeOfGlobalColorTable;
    data[5] = backgroundColorIndex;
    data[6] = pixelAspectRatio;

    file.write(reinterpret_cast<char*>(data), 7*sizeof(byte));

    return 0;
}