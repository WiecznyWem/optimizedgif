#include "Dictionary.h"

Dictionary::Dictionary(unsigned short initialCodeSize) 
    : initialCodeSize(initialCodeSize), cc(1<<(initialCodeSize-1)),  eoiCode(cc + 1), nextCode(eoiCode + 1),
      codeSize(initialCodeSize)
{
	memory.resize(memorySize, eoiCode);
	for(int i = 0; i < maxValue; i++) 
		memory[eoiCode * maxValue + i ] = i;
}

unsigned short Dictionary::insert(unsigned short parent, byte value)
{
   memory[parent * maxValue + value] = nextCode; 
   return incrementNextCode();
}

unsigned short Dictionary::find(unsigned short parent, byte value)
{
	return memory[parent * maxValue + value];
}

unsigned short Dictionary::getCC() const
{
    return cc;
}

unsigned short Dictionary::getEOI() const
{
    return eoiCode;
}

unsigned short Dictionary::getNextCode() const
{
    return nextCode;
}

unsigned short Dictionary::getCodeSize() const
{
    return codeSize;
}

void Dictionary::reset()
{
    nextCode = eoiCode + 1;
    codeSize = initialCodeSize;
	std::fill(memory.begin(), memory.end(), eoiCode);
    for(int i = 0; i < maxValue; i++) 
        memory[eoiCode * maxValue + i] = i;
}

unsigned short Dictionary::incrementNextCode()
{
    int old = nextCode;
    nextCode += 1;

    codeSize = 1;
    int tmp = old;
    while(tmp >>= 1) ++codeSize;

    if(nextCode == (1<<maxCodeSize))
        nextCode = cc;

    return old;
}
