#ifndef CROSS_BYTE_BUILDER_H
#define CROSS_BYTE_BUILDER_H

#include "../Common.h"
#include <vector>

class CrossByteBuilder
{
public:
    CrossByteBuilder(unsigned short reserve = 0);
    void push_back(unsigned short value, byte size);
    std::vector<byte> getData();
    
private:
    std::vector<byte> data;
    byte unused;
};

#endif
