#ifndef GIF_ENCODER_H
#define GIF_ENCODER_H

#include "../Common.h"
#include "LzwEncoder.h"
#include "GifSections/Header.h"
#include "GifSections/LogicalScreenDescriptor.h"
#include "GifSections/ImageDescriptor.h"
#include "GifSections/ColorTable.h"
#include "GifSections/RasterData.h"
#include "GifSections/Trailer.h"
#include "GifSections/GraphicControlExtension.h"
#include "GifSections/NetscapeLoopingApplicationExtension.h"

class GifEncoder
{
public:
    GifEncoder( const std::string filename, 
                const int width, 
                const int height, 
                const unsigned short colors, 
                const bool looped); 
    void addFrame(  const std::vector<RGBColor>& colorTable, 
                    byte* frame, 
                    const unsigned short width, 
                    const unsigned short height, 
                    const unsigned short leftOffset, 
                    const unsigned short topOffset, 
                    const unsigned short delay);
    void close();

private: 
    const int width;
    const int height;
    unsigned short colors;
    const bool looped;
    byte colorBits;
    std::ofstream file;
};

#endif
