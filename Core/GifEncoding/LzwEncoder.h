#ifndef LZW_ENCODER
#define LZW_ENCODER

#include "../Common.h"
#include "Dictionary.h"
#include "CrossByteBuilder.h"
#include <vector>

class LzwEncoder
{
public:
    static std::vector<byte> encode(const byte* data, int dataSize, byte minimumCodeSize);
};

#endif
