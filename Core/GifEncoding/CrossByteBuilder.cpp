#include "CrossByteBuilder.h"

CrossByteBuilder::CrossByteBuilder(unsigned short reserve)
{
    data.reserve(reserve);
    unused = 0;
}

void CrossByteBuilder::push_back(unsigned short value, byte size)
{
    unsigned short remainder = value;
    while(size != 0)
    {
        if(unused == 0)
        {
            unused = 8;
            data.push_back(0);
        }

        unsigned short cropped = remainder;
 
        if(size > unused)
        {
            cropped &= (1 << unused + 1) - 1;
            cropped <<= 8 - unused;
            
            remainder >>= unused;
            size -= unused;
            unused = 0;
        } else
        {
            cropped <<= 8 - unused;

            unused -= size;
            size = 0;
        }
        data.back() |= cropped;
    }
}

std::vector<byte> CrossByteBuilder::getData()
{
    return std::move(data);
}