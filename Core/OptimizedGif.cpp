#include "OptimizedGif.h"

#include "Common.h"
#include "GifEncoding/GifEncoder.h"
#include "ColorQuantization/IColorQuantizer.h"
#include "ColorQuantization/ModularColorQuantizer.h"
#include "ColorQuantization/GPUColorQuantizer.h"
#include "ColorQuantization/Exact.h"
#include "ColorQuantization/KMeans.h"
#include "ColorQuantization/NoDither.h"
#include "ColorQuantization/HorizontalDither.h"
#include "Optimization/FrameCollector.h"
#include "Optimization/Optimizer.h"
#include <fstream>
#include <string>
#include <algorithm>
#ifdef OpenCL_ENABLED
#include <CL/cl.h>
#endif // OpenCL_ENABLED

OptimizedGif::OptimizedGif(std::string filename, int width, int height, unsigned short colors, bool looped) 
    : width(width), height(height)
{
    ge = new GifEncoder(filename, width, height, colors, looped);
    fc = new FrameCollector();
    
    colors += 1;
    if(colors > 256)
        colors = 256;
    this->colors = colors;

#ifdef OpenCL_ENABLED
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;   
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
    ret = clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, 1, &device_id, &ret_num_devices);
#endif // OpenCL_ENABLED
}

OptimizedGif::~OptimizedGif()
{
    delete ge;
    delete fc;
}

void OptimizedGif::addFrame(byte* frame, unsigned int delay)
{
    std::vector<RGBColor> colorTable;
    std::vector<byte> quantizedFrame;
	std::vector<unsigned int> colorCounter;
    ModularColorQuantizer<Exact, NoDither> cqExact(colors, width, height, frame, fc);
    if(cqExact.quantize() == 0)
    {
        colorTable = cqExact.returnColorTable();
        quantizedFrame = cqExact.returnColoredImage();
        colorCounter = cqExact.returnColorCounter();
    } else
    {
        IColorQuantizer* cq;
#ifndef OpenCL_ENABLED
	// TODO add compiler option to Mathematica's gcc to set this define
        cq = new ModularColorQuantizer<KMeans, HorizontalDither>(colors, width, height, frame, fc);
        ((ModularColorQuantizer<KMeans, HorizontalDither>*)cq)->setPersistenceThreshold(3);
#else
        cq = new GPUColorQuantizer(colors, width, height, frame, fc);    
#endif // OpenCL_ENABLED
        cq->quantize();
        colorTable = cq->returnColorTable();
        quantizedFrame = cq->returnColoredImage();
        colorCounter = cq->returnColorCounter();
        delete cq;
    }

    byte* finalFrame = quantizedFrame.data();
    unsigned short finalWidth = width;
    unsigned short finalHeight = height;
    unsigned short finalLeftOffset = 0;
    unsigned short finalTopOffset = 0;
	Optimizer optimizer(width, height, quantizedFrame.data(), colorTable, fc); 
    if(fc->getSize()>0)
    {
        optimizer.optimize();
        finalFrame = optimizer.output.data();
        finalWidth = optimizer.outputWidth;
        finalHeight = optimizer.outputHeight;
        finalLeftOffset = optimizer.offsetLeft;
        finalTopOffset = optimizer.offsetTop;
    }
    ge->addFrame(colorTable, finalFrame, finalWidth, finalHeight, finalLeftOffset, finalTopOffset, delay);
    fc->push(quantizedFrame, colorTable, colorCounter);
}

void OptimizedGif::close()
{
    ge->close();
}
