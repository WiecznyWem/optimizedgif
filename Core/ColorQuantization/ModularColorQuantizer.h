#ifndef MODULAR_COLOR_QUANTIZER_H
#define MODULAR_COLOR_QUANTIZER_H

#include "IColorQuantizer.h"
#include "IQuantizationMethod.h"

template<typename qmethod, typename dmethod>
class ModularColorQuantizer : public IColorQuantizer
{
static_assert(std::is_base_of<IQuantizationMethod, qmethod>::value, "qmethod must derive from IQuantizationMethod");    
static_assert(std::is_base_of<IQuantizationMethod, qmethod>::value, "qmethod must derive from IQuantizationMethod");    

public:
    ModularColorQuantizer(const unsigned short numberOfColors, 
                   const unsigned short width, 
                   const unsigned short height, 
                   byte* frame,
                   const FrameCollector* fc = NULL)
        : IColorQuantizer(numberOfColors, width, height, frame, fc),
		quantizeMethod(numberOfColors, width, height, frame, fc),
        ditherMethod(quantizeMethod, width, height, frame, fc)
    {
    }

    int quantize()
    {
        int code = quantizeMethod.quantize();
        if(code != 0) return code;
        code = ditherMethod.dither();
        return code;
    }

    std::vector<RGBColor> returnColorTable()
    {
        return quantizeMethod.returnColorTable();
    }

    std::vector<byte> returnColoredImage()
    {
        return ditherMethod.returnColoredImage();
    }

	std::vector<unsigned int> returnColorCounter()
	{
		return std::vector<unsigned int>();
	}

    void setPersistenceThreshold(unsigned short threshold)
    {
        quantizeMethod.setPersistenceThreshold(threshold);
    }

protected:
    qmethod quantizeMethod;
    dmethod ditherMethod;
};

#endif
