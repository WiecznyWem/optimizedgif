#ifdef OpenCL_ENABLED
#ifndef GPU_COLOR_QUANTIZER
#define GPU_COLOR_QUANTIZER

#include <vector>
#include "IColorQuantizer.h"
#include "GPUKernels.h"

class GPUColorQuantizer : public IColorQuantizer
{
public:
    GPUColorQuantizer(const unsigned short numberOfColors, 
                   const unsigned short width, 
                   const unsigned short height, 
                   byte* frame,
                   const FrameCollector* fc);
    ~GPUColorQuantizer();
	int quantize();
    std::vector<RGBColor> returnColorTable();
	std::vector<unsigned int> returnColorCounter();
    std::vector<byte> returnColoredImage();
private:
	GPUKernels& kernels; 
	std::string kernelsSourceCode;
	std::string getSourceCode();	
	std::vector<RGBColor> colorTable;
	std::vector<unsigned int> colorCounter;
	std::vector<byte> coloredImage;	

	size_t up(size_t v, size_t m);
	unsigned int imageSize;
    unsigned int localWorkSizeX;
    unsigned int localWorkSizeY;
    unsigned int accGroupsX;
    unsigned int accGroupsY;
    unsigned int accGroupsNumber;
    cl_context& context;
    cl_command_queue& command_queue;
    cl_int ret;

    cl_mem inputImageClBuffer;
	cl_mem coloredImageClBuffer; 
	cl_mem partialSumsClBuffer; 
    cl_mem colorTableClBuffer;
	cl_mem colorCounterClBuffer;
    size_t imageOrigin[3];
    size_t imageRegion[3];
    size_t acc_local_work_size[3];
    size_t acc_global_work_size[3];
    size_t part_local_work_size[3];
    size_t part_global_work_size[3];
    size_t quant_local_work_size[3];
    size_t quant_global_work_size[3];
    cl_image_format clImageFormat;
    cl_image_desc clImageDesc;

	void createBufferObjects();
	void setupKernels();
    void iterateAccumulation();
    void iteratePartition();
    void quantizeImage();
	void getColorTableFromGPU();
	void getQuantizedImageFromGPU();

	unsigned short workers;
	unsigned short batchSize;
	unsigned short horizontalGroups;

	unsigned short persistenceLevel;
};

#endif //GPU_COLOR_QUANTIZER
#endif // OpenCL_ENABLED
