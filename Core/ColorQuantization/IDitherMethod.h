#ifndef I_DITHER_METHOD
#define I_DITHER_METHOD

#include "../Common.h"
#include "../Optimization/FrameCollector.h"
#include "IQuantizationMethod.h"
#include <vector>

class IDitherMethod
{
public:
    IDitherMethod(  const IQuantizationMethod& qmethod, 
                    const unsigned short width, 
                    const unsigned short height, 
                    const byte* frame,
                    const FrameCollector* fc);

    virtual int dither() = 0;
    std::vector<byte> returnColoredImage();

protected:
    const IQuantizationMethod& qmethod;
    const unsigned short width;
    const unsigned short height; 
    const byte* frame;
    const FrameCollector* fc;
    
    std::vector<byte> coloredImage;
    virtual void setup();
};

#endif