#include "IQuantizationMethod.h"

IQuantizationMethod::IQuantizationMethod(   const unsigned short numberOfColors, 
                                            const unsigned short width, 
                                            const unsigned short height, 
                                            const byte* frame,
                                            const FrameCollector* fc)
    : numberOfColors(numberOfColors), width(width), height(height), frame(frame), fc(fc),
    persistenceThreshold(DEFAULT_PERSISTENCE_THRESHOLD), persistenceLevel(0)
{

}

const std::vector<RGBColor>& IQuantizationMethod::getColorTable() const
{
    return colorTable;
}

std::vector<RGBColor> IQuantizationMethod::returnColorTable() 
{
    return std::move(colorTable);
}

void IQuantizationMethod::setPersistenceThreshold(unsigned short threshold)
{
    persistenceThreshold = threshold;
}

void IQuantizationMethod::setup()
{
    unsigned short sizeOfColorTable = 2;
    unsigned short tmp = numberOfColors - 1;
    while(tmp >>= 1)
        sizeOfColorTable <<= 1;

    colorTable = std::vector<RGBColor>(sizeOfColorTable);
    colorTable[0] = {0,0,0};
}

unsigned short IQuantizationMethod::getPersistenceLevel() const
{
    return persistenceLevel;
}

void IQuantizationMethod::fillWithPersistentColors()
{
    // TODO: zoptymalizować to
    std::vector<unsigned int> counter(numberOfColors);
    std::vector<unsigned int> sortHelper(numberOfColors);

    if(fc->getSize() > 0)
    {
        const std::vector<RGBColor>& prevTable = *fc->getTable(fc->getSize()-1);
        const std::vector<byte>& prevFrame = *fc->getFrame(fc->getSize()-1);

        for(unsigned int i = 0; i < width * height; i++)
        {
            RGBColor previousColor = prevTable[prevFrame[i]];
            RGBColor currentColor = {frame[3*i], frame[3*i+1], frame[3*i+2]};

            byte previousCode = findColor(previousColor);
            byte currentCode = findColor(currentColor);

            if(previousCode == currentCode)
                ++counter[currentCode];
        }

        std::iota(sortHelper.begin()+1, sortHelper.end(), 1);
        std::sort(sortHelper.begin()+1, sortHelper.end(), 
            [this, &counter](const unsigned int& lhs, const unsigned int& rhs) -> bool 
            {
                return counter[lhs] > counter[rhs];
            }
        ); 
    
        for(unsigned short i = 1; i < numberOfColors; i++)
        {
            colorTable[i] = (*fc->getTable(0))[sortHelper[i]];

            if(counter[sortHelper[i]] < 3)
            {
                persistenceLevel = i;
                break;
            }
        } 
    }
}