#ifndef I_COLOR_QUANTIZER
#define I_COLOR_QUANTIZER

#include "../Common.h"
#include "../Optimization/FrameCollector.h"

class IColorQuantizer
{
protected:
    const unsigned short numberOfColors;
    const unsigned short width;
    const unsigned short height; 
    byte* frame;
    const FrameCollector* fc;
public:
	IColorQuantizer(const unsigned short numberOfColors, 
                   const unsigned short width, 
                   const unsigned short height, 
                   byte* frame,
                   const FrameCollector* fc)
        : numberOfColors(numberOfColors), width(width), height(height), frame(frame), fc(fc)
    {
    }

    virtual int quantize() = 0;
    virtual std::vector<RGBColor> returnColorTable() = 0;
    virtual std::vector<byte> returnColoredImage() = 0;
	virtual std::vector<unsigned int> returnColorCounter() = 0;
};

#endif
