#ifndef KMEANS_H
#define KMEANS_H

#include "../Common.h"
#include "IQuantizationMethod.h"
#include <cmath>

class KMeans : public IQuantizationMethod
{
public:
    KMeans(  const unsigned short numberOfColors, 
            const unsigned short width, 
            const unsigned short height, 
            const byte* frame,
            const FrameCollector* fc);

    byte findColor(const RGBColor& trueColor) const;

    int quantize();

private:
    static std::vector<double> totalR;
    static std::vector<double> totalG;
    static std::vector<double> totalB;
    static std::vector<unsigned int> numbers;

};

#endif