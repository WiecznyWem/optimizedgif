#ifndef I_QUANTIZATION_METHOD_H
#define I_QUANTIZATION_METHOD_H

#include "../Common.h"
#include "../Optimization/FrameCollector.h"
#include <vector>
#include <numeric>
#include <algorithm>

class IQuantizationMethod
{
public:
    IQuantizationMethod(const unsigned short numberOfColors, 
                        const unsigned short width, 
                        const unsigned short height, 
                        const byte* frame,
                        const FrameCollector* fc);

    virtual byte findColor(const RGBColor& trueColor) const = 0;
    virtual int quantize() = 0;
    const std::vector<RGBColor>& getColorTable() const;
    std::vector<RGBColor> returnColorTable();

    void setPersistenceThreshold(unsigned short threshold);

protected:
    const unsigned short numberOfColors;
    const unsigned short width;
    const unsigned short height; 
    const byte* frame;
    const FrameCollector* fc;
    
    std::vector<RGBColor> colorTable;
    virtual void setup();
    void fillWithPersistentColors();
    unsigned short getPersistenceLevel() const;

private:    
    unsigned short persistenceThreshold;
    unsigned short persistenceLevel;
};

#endif