#include "GPUColorQuantizer.h"
#ifdef OpenCL_ENABLED

#include <iostream>
#include <cmath>
#include <numeric>
#include <algorithm>

GPUColorQuantizer::GPUColorQuantizer(const unsigned short numberOfColors, 
                   const unsigned short width, 
                   const unsigned short height, 
                   byte* frame,
                   const FrameCollector* fc = NULL)
	: IColorQuantizer(numberOfColors, width, height, frame, fc), localWorkSizeX(32), localWorkSizeY(localWorkSizeX),
	  kernels(GPUKernels::getInstance()), context(kernels.getContext()), command_queue(kernels.getCommandQueue())
{
	unsigned short sizeOfColorTable = 2;
    unsigned short tmp = numberOfColors - 1;
    while(tmp >>= 1)
        sizeOfColorTable <<= 1;
    colorTable.resize(sizeOfColorTable);
	colorTable[0] = {0,0,0};
	colorCounter.resize(sizeOfColorTable);

	if(fc->getSize() == 0)
		persistenceLevel = 1;
	else
	{
	    std::vector<unsigned int> sortHelper(numberOfColors);
	    const std::vector<unsigned int>* counter = fc->getCounter(0);
	    std::iota(sortHelper.begin()+1, sortHelper.end(), 1);
	    std::sort(sortHelper.begin()+1, sortHelper.end(), 
	        [this, counter](const unsigned int& lhs, const unsigned int& rhs) -> bool 
	        {
	            return (*counter)[lhs] > (*counter)[rhs];
	        }
	    ); 
		for(int i = 1; i < numberOfColors; i++)
		{
			if((*counter)[sortHelper[i]] < 0.005 * width * height )
			{
				persistenceLevel = i;
				break;
			}
		}

		for(int i = 1; i < persistenceLevel; i++)
			colorTable[i] = (*fc->getTable(0))[sortHelper[i-1]];
	}
	for(int i = persistenceLevel; i < numberOfColors; i++)
	{	
		byte g = 255.0 / (numberOfColors - persistenceLevel) * (i - persistenceLevel);
		colorTable[i] = {g,g,g};
	}

    accGroupsX = up(width, localWorkSizeX) / localWorkSizeX;
    accGroupsY = up(height, localWorkSizeY) / localWorkSizeY;
    accGroupsNumber = accGroupsX * accGroupsY;

	imageSize = width * height;	
    coloredImage.resize(imageSize);
	for(int i = 0; i < imageSize; i++)
		coloredImage[i] = 1;

	if(!kernels.has("accumulate"))
		kernels.add(getSourceCode(), {"accumulate", "partition", "quantize"});

	workers = 16;
	batchSize = 4 * workers;	
	horizontalGroups = ceil(1.0 * width / batchSize);
    createBufferObjects();
    clFinish(command_queue);
    setupKernels();
    clFlush(command_queue);
    clFinish(command_queue);
};

GPUColorQuantizer::~GPUColorQuantizer()
{
    clReleaseMemObject(colorTableClBuffer);
    clReleaseMemObject(partialSumsClBuffer);
    clReleaseMemObject(inputImageClBuffer);
    clReleaseMemObject(coloredImageClBuffer);
    clReleaseMemObject(colorCounterClBuffer);
    clFlush(command_queue);
    clFinish(command_queue);
}
int GPUColorQuantizer::quantize()
{
	for(int i = 0; i < 8; i++)
	{
		iterateAccumulation();
		iteratePartition();
    }
	quantizeImage();
	
	getQuantizedImageFromGPU();
	getColorTableFromGPU();
    return 0;
}

std::vector<RGBColor> GPUColorQuantizer::returnColorTable()
{
	return std::move(colorTable);
}

std::vector<unsigned int> GPUColorQuantizer::returnColorCounter()
{
	return std::move(colorCounter);
}

std::vector<byte> GPUColorQuantizer::returnColoredImage()
{
	return std::move(coloredImage);
};

inline size_t GPUColorQuantizer::up(size_t v, size_t m = 32)
{
    return v + m - (v % m);
}

std::string GPUColorQuantizer::getSourceCode()
{
	return 
	"uchar colorize(ushort colorTableSize, __global uchar* colorTable, uint3 color) \n"
	"{ \n"
	"    uchar bestIndex = 0; \n"
	"    float bestValue = FLT_MAX; \n"
	"	 \n"
	"	if(!(color.x == " + std::to_string(TRANSPARENT.r) + " && color.y == " + std::to_string(TRANSPARENT.g) + " && color.z == " + std::to_string(TRANSPARENT.b) + "))   \n"
	"	{\n"
	"	    for(uint i = 1; i < colorTableSize; i++) \n"
	"	    { \n"
	"	        float value = (colorTable[3*i] - color.x) * (colorTable[3*i] - color.x) + \n"
	"	                      (colorTable[3*i + 1] - color.y) * (colorTable[3*i + 1] - color.y) + \n"
	"	                      (colorTable[3*i + 2] - color.z) * (colorTable[3*i + 2] - color.z);  \n"
	"	        if(value < bestValue) \n"
	"	        { \n"
	"	            bestValue = value; \n"
	"	            bestIndex = i; \n"
	"	        } \n"
	"	    } \n"
	"	 \n"
	"	}\n"
	"    return bestIndex; \n"
	"} \n"
	" \n"
	"__kernel void quantize( \n"
	"        __global uchar* image, \n"
	"        uint imageSize,  \n"
	" 		 ushort colorTableSize, \n"
	"        __global uchar* colorTable, \n"
	"        __global uchar* coloredImage, \n"
	"      	ushort imageWidth, \n"
	"		ushort batchSize, \n"
	"		ushort horizontalGroups,\n"
	"		__local int* bonuses,\n"
	"		__global uint* colorCounter \n"
	"    ) \n"
	"{ \n"
	"    uint local_y = get_local_id(0); \n"
	"    uint workers = get_local_size(0); \n"
	"    uint group_id = get_group_id(0); \n"
	"    uint group_y = group_id / (  horizontalGroups); \n"
	"    uint group_x = group_id % (  horizontalGroups ); \n"
	"    uint global_y = group_y * workers + local_y; \n"
	"    for(uint k = 0; k < batchSize + 2 * workers; k++){ \n"
	"    	uint local_x = k - 2*local_y; \n"
	"    	if(local_x < 0 || local_x >= batchSize) continue; \n"
	"    	uint global_x = group_x * batchSize + local_x; \n"
	"		if(global_x > imageWidth) continue; \n"
	"    	uint pixel_id = global_y * imageWidth + global_x; \n"
	"   	if(pixel_id >= imageSize) continue; \n"
	"		float3 fpx = (float3)(image[3*pixel_id] + bonuses[3*(local_y * batchSize + local_x) + 0], image[3*pixel_id+1] + bonuses[3*(local_y * batchSize + local_x) + 1], image[3*pixel_id+2] + bonuses[3*(local_y * batchSize + local_x) + 2] );\n"
	"    	if(fpx.x > 255) fpx.x = 255; if(fpx.y > 255) fpx.y = 255; if(fpx.z > 255) fpx.z = 255; \n"
	"    	if(fpx.x < 0) fpx.x = 0; if(fpx.y < 0) fpx.y = 0; if(fpx.z < 0) fpx.z = 0; \n"
	"    	uint3 px = (uint3)(fpx.x, fpx.y, fpx.z); \n"
	"    	uchar index = colorize(colorTableSize, colorTable, px); \n"
	"    	float3 bonus = (float3)(image[3*pixel_id + 0] - colorTable[3*index], image[3*pixel_id + 1] - colorTable[3*index + 1], image[3*pixel_id + 2] - colorTable[3*index + 2]); \n"
	"	 	coloredImage[pixel_id] = index;\n"
	"	    atomic_add(colorCounter + index, 1); \n"
	"		barrier(CLK_LOCAL_MEM_FENCE);\n"
	"		if(local_x + 1 < batchSize) {\n"	
	"    		bonuses[3*(local_y * batchSize + (local_x+1)) + 0] += 7.0 / 16.0 * bonus.x; \n"
	"    		bonuses[3*(local_y * batchSize + (local_x+1)) + 1] += 7.0 / 16.0 * bonus.y; \n"
	"   	 	bonuses[3*(local_y * batchSize + (local_x+1)) + 2] += 7.0 / 16.0 * bonus.z; \n"
	"	    } \n"
	"		if(local_x + 1 < batchSize && local_y + 1 < workers) {\n"	
	"    		bonuses[3*((local_y+1) * batchSize + (local_x+1)) + 0] += 1.0 / 16.0 * bonus.x; \n"
	"    		bonuses[3*((local_y+1) * batchSize + (local_x+1)) + 1] += 1.0 / 16.0 * bonus.y; \n"
	"   	 	bonuses[3*((local_y+1) * batchSize + (local_x+1)) + 2] += 1.0 / 16.0 * bonus.z; \n"
	"	    } \n"
	"		if(local_y + 1 < workers) {\n"	
	"    		bonuses[3*((local_y+1) * batchSize + (local_x)) + 0] += 5.0 / 16.0 * bonus.x; \n"
	"    		bonuses[3*((local_y+1) * batchSize + (local_x)) + 1] += 5.0 / 16.0 * bonus.y; \n"
	"   	 	bonuses[3*((local_y+1) * batchSize + (local_x)) + 2] += 5.0 / 16.0 * bonus.z; \n"
	"	    } \n"
	"		if(local_x - 1 >= 0 && local_y + 1 < workers) {\n"	
	"    		bonuses[3*((local_y+1) * batchSize + (local_x-1)) + 0] += 3.0 / 16.0 * bonus.x; \n"
	"    		bonuses[3*((local_y+1) * batchSize + (local_x-1)) + 1] += 3.0 / 16.0 * bonus.y; \n"
	"   	 	bonuses[3*((local_y+1) * batchSize + (local_x-1)) + 2] += 3.0 / 16.0 * bonus.z; \n"
	"	    } \n"
	"    } \n"
	"} \n"
	" \n"
	"__kernel void accumulate( \n"
	"        __global uchar* image, \n"
	"		 uint imageSize, \n"
	"        ushort colorTableSize, \n"
	"        __global uchar* colorTable, \n"
	"        __global uint* partialSums \n"
	"		) { \n"
	" 	 uint id = get_global_id(0); \n"
	"    if(id >= imageSize) \n"
	"        return; \n"
	"    uint localId = get_local_id(0); \n"
	"    uint groupId = get_group_id(0); \n"
	"    uint groups = get_num_groups(0); \n"
	"    uint groupSize = get_local_size(0); \n"
	" \n"
	"    uint3 px = (uint3)(image[3*id + 0], image[3*id + 1], image[3*id + 2]); \n"
	"    uchar bestColorIndex = colorize(colorTableSize, colorTable, px); \n"
	" \n"
	"    atomic_add(partialSums + 4*bestColorIndex + 0, 1); \n"
	"    atomic_add(partialSums + 4*bestColorIndex + 1, px.x); \n"
	"    atomic_add(partialSums + 4*bestColorIndex + 2, px.y); \n"
	"    atomic_add(partialSums + 4*bestColorIndex + 3, px.z); \n"
	"} \n"
	" \n"
	"__kernel void partition( \n"
	"        ushort colorTableSize, \n"
	"        __global uchar* colorTable, \n"
	"        __global uint* partialSums, \n"
	"        uint accGroupsNumber, \n"
	"        uint accGroupsX, \n"
	"		 ushort persistenceLevel \n"
	"    ) \n"
	"{ \n"
	"    uint id = get_global_id(0) + persistenceLevel; \n"
	"    if(id >= colorTableSize) \n"
	"        return;  \n"
	" \n"
	"    uint counter = 0; \n"
	"    uint racc = 0; \n"
	"    uint gacc = 0; \n"
	"    uint bacc = 0; \n"
	" \n"
	"    counter = partialSums[4* id + 0];  \n"
	"    racc    = partialSums[4* id + 1];  \n"
	"    gacc    = partialSums[4* id + 2];  \n"
	"    bacc    = partialSums[4* id + 3];  \n"
	" \n"
	" \n"
	"    if(counter == 0) \n"
	"        counter = 1; \n"
	"    colorTable[3*id    ] = racc / counter; \n"
	"    colorTable[3*id + 1] = gacc / counter; \n"
	"    colorTable[3*id + 2] = bacc / counter; \n"
	"    partialSums[4* id + 0] = 0;  \n"
	"    partialSums[4* id + 1] = 0;  \n"
	"    partialSums[4* id + 2] = 0;  \n"
	"    partialSums[4* id + 3] = 0;  \n"
	"} \n"
	" \n"
	"";
}



void GPUColorQuantizer::createBufferObjects()
{
	inputImageClBuffer = clCreateBuffer(context,
                      CL_MEM_READ_ONLY,
                      sizeof(unsigned char)*width*height*3,
                      NULL,
                      &ret); trace(ret);

    ret = clEnqueueWriteBuffer(command_queue,
                           inputImageClBuffer, /*cl_mem buffer,*/
                           CL_TRUE, /*cl_bool blocking_write,*/
                           0, /* size_t offset, */
                           sizeof(unsigned char)*width*height*3, /*size_t size,*/
                           frame, /*const void *ptr,*/
                           0, /*cl_uint num_events_in_wait_list,*/
                           NULL, /* const cl_event *event_wait_list, */
                           NULL /*cl_event *event */); trace(ret);

    coloredImageClBuffer = clCreateBuffer(context,
                      CL_MEM_WRITE_ONLY,
                      sizeof(unsigned char)*coloredImage.size(),
                      coloredImage.data(),
                      &ret); trace(ret);
    ret = clEnqueueWriteBuffer(command_queue,
                           coloredImageClBuffer, /*cl_mem buffer,*/
                           CL_TRUE, /*cl_bool blocking_write,*/
                           0, /* size_t offset, */
                           sizeof(unsigned char)*coloredImage.size(), /*size_t size,*/
                           coloredImage.data(), /*const void *ptr,*/
                           0, /*cl_uint num_events_in_wait_list,*/
                           NULL, /* const cl_event *event_wait_list, */
                           NULL /*cl_event *event */); trace(ret);
    colorTableClBuffer = clCreateBuffer(context,
                      CL_MEM_READ_WRITE ,
                      sizeof(RGBColor)*colorTable.size(),
                      colorTable.data(),
                      &ret); trace(ret);
    ret = clEnqueueWriteBuffer(command_queue,
                           colorTableClBuffer, /*cl_mem buffer,*/
                           CL_TRUE, /*cl_bool blocking_write,*/
                           0, /* size_t offset, */
                           3*sizeof(byte)*colorTable.size(), /*size_t size,*/
                           colorTable.data(), /*const void *ptr,*/
                           0, /*cl_uint num_events_in_wait_list,*/
                           NULL, /* const cl_event *event_wait_list, */
                           NULL /*cl_event *event */); trace(ret);
    colorCounterClBuffer = clCreateBuffer(context,
                      CL_MEM_READ_WRITE ,
                      sizeof(unsigned int)*colorCounter.size(),
                      colorCounter.data(),
                      &ret); trace(ret);
    ret = clEnqueueWriteBuffer(command_queue,
                           colorCounterClBuffer, /*cl_mem buffer,*/
                           CL_TRUE, /*cl_bool blocking_write,*/
                           0, /* size_t offset, */
                           sizeof(unsigned int)*colorCounter.size(), /*size_t size,*/
                           colorCounter.data(), /*const void *ptr,*/
                           0, /*cl_uint num_events_in_wait_list,*/
                           NULL, /* const cl_event *event_wait_list, */
                           NULL /*cl_event *event */); trace(ret);
    partialSumsClBuffer = clCreateBuffer(context,
                      CL_MEM_READ_WRITE,
                      sizeof(unsigned int)*4*numberOfColors,
                      NULL,
                      &ret); trace(ret);
}

void GPUColorQuantizer::setupKernels()
{
    acc_local_work_size[0] = localWorkSizeX * localWorkSizeY;
    acc_local_work_size[1] = 1;
    acc_local_work_size[2] = 1;
    acc_global_work_size[0] = up(imageSize, localWorkSizeX * localWorkSizeY);
    acc_global_work_size[1] = 1;
    acc_global_work_size[2] = 1;
    ret = clSetKernelArg(kernels.get("accumulate"), 0, sizeof(cl_mem), (void *)&inputImageClBuffer);trace(ret);
    ret = clSetKernelArg(kernels.get("accumulate"), 1, sizeof(unsigned int), (void *)&imageSize);trace(ret);
    ret = clSetKernelArg(kernels.get("accumulate"), 2, sizeof(unsigned short), (void *)&numberOfColors);trace(ret);
    ret = clSetKernelArg(kernels.get("accumulate"), 3, sizeof(cl_mem), (void *)&colorTableClBuffer);trace(ret);
    ret = clSetKernelArg(kernels.get("accumulate"), 4, sizeof(cl_mem), (void *)&partialSumsClBuffer);trace(ret);

    part_local_work_size[0] = localWorkSizeX;
    part_local_work_size[1] = 1; 
    part_local_work_size[2] = 1;
    part_global_work_size[0] = up(numberOfColors, localWorkSizeX);
    part_global_work_size[1] = 1;
    part_global_work_size[2] = 1;
    ret = clSetKernelArg(kernels.get("partition"), 0, sizeof(unsigned short), (void *)&numberOfColors);trace(ret);
    ret = clSetKernelArg(kernels.get("partition"), 1, sizeof(cl_mem), (void *)&colorTableClBuffer);trace(ret);
    ret = clSetKernelArg(kernels.get("partition"), 2, sizeof(cl_mem), (void *)&partialSumsClBuffer);trace(ret);
    ret = clSetKernelArg(kernels.get("partition"), 3, sizeof(unsigned int), (void *)&accGroupsNumber);trace(ret);
    ret = clSetKernelArg(kernels.get("partition"), 4, sizeof(unsigned int), (void *)&accGroupsX);trace(ret);
    ret = clSetKernelArg(kernels.get("partition"), 5, sizeof(unsigned short), (void *)&persistenceLevel);trace(ret);
    
    quant_local_work_size[0] = workers;
    quant_local_work_size[1] = 1; 
    quant_local_work_size[2] = 1;
    quant_global_work_size[0] = up(height, workers) * horizontalGroups;
    quant_global_work_size[1] = 1;
    quant_global_work_size[2] = 1;
    ret = clSetKernelArg(kernels.get("quantize"), 0, sizeof(cl_mem), (void *)&inputImageClBuffer);trace(ret);
    ret = clSetKernelArg(kernels.get("quantize"), 1, sizeof(unsigned int), (void *)&imageSize);trace(ret);
    ret = clSetKernelArg(kernels.get("quantize"), 2, sizeof(unsigned short), (void *)&numberOfColors);trace(ret);
    ret = clSetKernelArg(kernels.get("quantize"), 3, sizeof(cl_mem), (void *)&colorTableClBuffer);trace(ret);
    ret = clSetKernelArg(kernels.get("quantize"), 4, sizeof(cl_mem), (void *)&coloredImageClBuffer);trace(ret);
    ret = clSetKernelArg(kernels.get("quantize"), 5, sizeof(unsigned short), (void *)&width);trace(ret);
    ret = clSetKernelArg(kernels.get("quantize"), 6, sizeof(unsigned short), (void *)&batchSize);trace(ret);
    ret = clSetKernelArg(kernels.get("quantize"), 7, sizeof(unsigned short), (void*)&horizontalGroups);trace(ret);
    ret = clSetKernelArg(kernels.get("quantize"), 8, sizeof(float)*3*batchSize*workers, NULL);trace(ret);
    ret = clSetKernelArg(kernels.get("quantize"), 9, sizeof(cl_mem), (void*)&colorCounterClBuffer);trace(ret);
    clFlush(command_queue);
    clFinish(command_queue);
}

void GPUColorQuantizer::iterateAccumulation()
{
    ret = clEnqueueNDRangeKernel(command_queue, kernels.get("accumulate"), 1, NULL, acc_global_work_size, acc_local_work_size, 0, NULL, NULL); trace(ret);
}

void GPUColorQuantizer::iteratePartition()
{

    ret = clEnqueueNDRangeKernel(command_queue, kernels.get("partition"), 1, NULL, part_global_work_size, part_local_work_size, 0, NULL, NULL); trace(ret);
}

void GPUColorQuantizer::quantizeImage()
{
    ret = clEnqueueNDRangeKernel(command_queue, kernels.get("quantize"), 1, NULL, quant_global_work_size, quant_local_work_size, 0, NULL, NULL); trace(ret);
}

void GPUColorQuantizer::getQuantizedImageFromGPU()
{
    clFlush(command_queue);
    clFinish(command_queue);
    ret = clEnqueueReadBuffer(command_queue,
                           coloredImageClBuffer, /*cl_mem buffer,*/
                           CL_TRUE, /*cl_bool blocking_write,*/
                           0, /* size_t offset, */
                           sizeof(unsigned char)*coloredImage.size(), /*size_t size,*/
                           coloredImage.data(), /*const void *ptr,*/
                           0, /*cl_uint num_events_in_wait_list,*/
                           NULL, /* const cl_event *event_wait_list, */
                           NULL /*cl_event *event */); trace(ret);
}

void GPUColorQuantizer::getColorTableFromGPU()
{
    ret = clEnqueueReadBuffer(command_queue,
                           colorTableClBuffer, /*cl_mem buffer,*/
                           CL_TRUE, /*cl_bool blocking_write,*/
                           0, /* size_t offset, */
                           3*sizeof(byte)*colorTable.size(), /*size_t size,*/
                           colorTable.data(), /*const void *ptr,*/
                           0, /*cl_uint num_events_in_wait_list,*/
                           NULL, /* const cl_event *event_wait_list, */
                           NULL /*cl_event *event */); trace(ret);
    ret = clEnqueueReadBuffer(command_queue,
                           colorCounterClBuffer, /*cl_mem buffer,*/
                           CL_TRUE, /*cl_bool blocking_write,*/
                           0, /* size_t offset, */
                           sizeof(unsigned int) * colorCounter.size(), /*size_t size,*/
                           colorCounter.data(), /*const void *ptr,*/
                           0, /*cl_uint num_events_in_wait_list,*/
                           NULL, /* const cl_event *event_wait_list, */
                           NULL /*cl_event *event */); trace(ret);
}
#endif // OpenCL_ENABLED
