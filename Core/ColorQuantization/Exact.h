#ifndef EXACT_H
#define EXACT_H

#include "IQuantizationMethod.h"
#include <set>
#include <algorithm>

class Exact : public IQuantizationMethod
{
public:
    Exact(  const unsigned short numberOfColors, 
            const unsigned short width, 
            const unsigned short height, 
            const byte* frame,
            const FrameCollector* fc);

    byte findColor(const RGBColor& trueColor) const;

    int quantize();
};


#endif