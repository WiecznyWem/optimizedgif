#include "GPUKernels.h"
#ifdef OpenCL_ENABLED
#include <iostream>

GPUKernels::GPUKernels()
{
    ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms); trace(ret);
    ret = clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, 1, &device_id, &ret_num_devices); trace(ret);
    context = clCreateContext( NULL, 1, &device_id, NULL, NULL, &ret); trace(ret);
    command_queue = clCreateCommandQueueWithProperties(context, device_id, 0, &ret); trace(ret);
}

GPUKernels::~GPUKernels()
{
	for(auto kernel : kernels)
	{
		clReleaseKernel(kernel.second);
	}
	clReleaseProgram(program);
    clReleaseCommandQueue(command_queue);
    clReleaseContext(context);
}

GPUKernels& GPUKernels::getInstance()
{
	static GPUKernels instance;
	return instance; 
}

void GPUKernels::add(std::string source, std::vector<std::string> names)
{
    std::vector<std::string> sources;
    sources.push_back(source);
	OpenCLProgram oclprogram = prepareSourcesForCL(sources);

    program = clCreateProgramWithSource(context, oclprogram.filesize, oclprogram.strings.data(), oclprogram.lengths.data(), &ret); trace(ret);
    ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL); trace(ret);
    if(ret != 0)
    {
        size_t len;
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, nullptr, &len);
       	char* logs = new char[len];
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, len, logs, NULL);
        std::cout<<"Kernel compilation error: "<<std::endl<<logs<<std::endl;
		delete logs;
    }
	// accumulate; partition; quantize
	for(auto name : names)
	{
		kernels[name] = clCreateKernel(program, name.c_str(), &ret); trace(ret);
	}
}

bool GPUKernels::has(std::string name)
{
	return kernels.find(name) != kernels.end();
}	

cl_kernel& GPUKernels::get(std::string name)
{
	return kernels[name];
}

struct GPUKernels::OpenCLProgram GPUKernels::prepareSourcesForCL(const std::vector<std::string>& sources)
{
    std::vector<size_t> lengths(sources.size());
    std::vector<const char*> strings(sources.size());

    for(int i = 0; i < sources.size(); i++)
    {
        strings[i] = sources[i].c_str();
        lengths[i] = sources[i].size();
    }

    return {sources.size(), std::move(strings), std::move(lengths)};
}


cl_command_queue& GPUKernels::getCommandQueue()
{
	return command_queue;
}

cl_context& GPUKernels::getContext()
{
	return context;
}
#endif //OpenCL_ENABLED
