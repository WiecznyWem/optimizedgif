#ifndef HORIZONTAL_DITHER_H
#define HORIZONTAL_DITHER_H

#include "IDitherMethod.h"

class HorizontalDither : public IDitherMethod
{
public:
    HorizontalDither(   const IQuantizationMethod& qmethod, 
                const unsigned short width, 
                const unsigned short height, 
                const byte* frame,
                const FrameCollector* fc);

    int dither();
};

#endif