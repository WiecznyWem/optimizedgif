#include "Exact.h"

Exact::Exact(   const unsigned short numberOfColors, 
                const unsigned short width, 
                const unsigned short height, 
                const byte* frame,
                const FrameCollector* fc)
    : IQuantizationMethod(numberOfColors, width, height, frame, fc)
{
    
}

byte Exact::findColor(const RGBColor& trueColor) const
{
    byte best = 0;
    if(!(trueColor.r == TRANSPARENT.r && trueColor.g == TRANSPARENT.g && trueColor.b == TRANSPARENT.b))
    {
        auto iter = std::upper_bound(colorTable.begin(), colorTable.end(), trueColor);
        best = iter - colorTable.begin() - 1;
    }
    
    return best;
}

int Exact::quantize()
{
    std::set<RGBColor> colors;
    for(unsigned int i = 0; i < width * height; i++)
    {
        RGBColor trueColor = {frame[3*i], frame[3*i+1], frame[3*i+2]};
        if(!(trueColor == TRANSPARENT))
            colors.insert(trueColor);

        if(colors.size() >= numberOfColors)
            return 1;
    }

    setup();

    unsigned int iter = 1;
    for( RGBColor color : colors)
    {
        colorTable[iter] = color;
        ++iter;
    }

    std::sort(colorTable.begin(), colorTable.end());

    return 0;
}