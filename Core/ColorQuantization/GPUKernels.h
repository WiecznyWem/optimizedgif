#ifndef GPU_KERNELS
#define GPU_KERNELS
#ifdef OpenCL_ENABLED

#define CL_TARGET_OPENCL_VERSION 220
#include <CL/cl.h>
#include <vector>
#include <map>
#include "../Common.h"
class GPUKernels
{
private:
	GPUKernels();
	std::map<std::string, cl_kernel> kernels;
    
	cl_platform_id platform_id;
    cl_device_id device_id;   
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_context context;
    cl_command_queue command_queue;
    cl_program program;
    cl_int ret;
public:
	static GPUKernels& getInstance();
	~GPUKernels();
	void add(std::string source, std::vector<std::string> names);

	bool has(std::string name);	
	cl_kernel& get(std::string name);
	cl_command_queue& getCommandQueue();
	cl_context& getContext();
private:

	struct OpenCLProgram
	{
	    size_t filesize;
	    std::vector<const char*> strings;
	    std::vector<size_t> lengths;
	};
	OpenCLProgram prepareSourcesForCL(const std::vector<std::string>& sources);
};

#endif // OpenCL_ENABLED
#endif // GPU_KERNELS
