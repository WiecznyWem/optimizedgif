#ifndef NO_DITHER_H
#define NO_DITHER_H

#include "IDitherMethod.h"

class NoDither : public IDitherMethod
{
public:
    NoDither(   const IQuantizationMethod& qmethod, 
                const unsigned short width, 
                const unsigned short height, 
                const byte* frame,
                const FrameCollector* fc);

    int dither();
};

#endif