#include "IDitherMethod.h"

IDitherMethod::IDitherMethod(   const IQuantizationMethod& qmethod,
                                const unsigned short width, 
                                const unsigned short height, 
                                const byte* frame,
                                const FrameCollector* fc)
    : qmethod(qmethod), width(width), height(height), frame(frame), fc(fc)
{

}

std::vector<byte> IDitherMethod::returnColoredImage() 
{
    return std::move(coloredImage);
}

void IDitherMethod::setup()
{
    coloredImage = std::vector<byte>();
    coloredImage.reserve(width*height);
}