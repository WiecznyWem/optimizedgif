#include "KMeans.h"

KMeans::KMeans(   const unsigned short numberOfColors,
                const unsigned short width, 
                const unsigned short height, 
                const byte* frame,
                const FrameCollector* fc)
    : IQuantizationMethod(numberOfColors, width, height, frame, fc)
{
    
}

std::vector<double> KMeans::totalR = std::vector<double>(MAX_COLORS);
std::vector<double> KMeans::totalG = std::vector<double>(MAX_COLORS);
std::vector<double> KMeans::totalB = std::vector<double>(MAX_COLORS);
std::vector<unsigned int> KMeans::numbers = std::vector<unsigned int>(MAX_COLORS);

byte KMeans::findColor(const RGBColor& trueColor) const
{
    byte best = 0;
    if(!(trueColor.r == TRANSPARENT.r && trueColor.g == TRANSPARENT.g && trueColor.b == TRANSPARENT.b))
    {
        best = 1;
        double minimalDist = RGBDistance(trueColor, colorTable[best]);
        for(unsigned short k = 2; k < numberOfColors; k++)
        {
            double cur = RGBDistance(trueColor, colorTable[k]);
            if(cur < minimalDist)
            {
                minimalDist = cur;
                best = k;
            }
        }
    }
    
    return best;
}

int KMeans::quantize()
{
    setup();
    fillWithPersistentColors();

    for(unsigned short i = getPersistenceLevel(); i < numberOfColors; i++)
    {
        byte k =  std::floor(255.0 * (i-getPersistenceLevel())/(numberOfColors-getPersistenceLevel()-1.0));
        colorTable[i] = {k,k,k};
    }

    for(int i = getPersistenceLevel(); i < numberOfColors; i++)
    {
        totalR[i] = 0;
        totalG[i] = 0;
        totalB[i] = 0;
        numbers[i] = 0;
    }


    for(unsigned short k = 0; k < 5; k ++)
    {
        for(unsigned int i = 0; i < width * height; i++)
        {
            RGBColor trueColor = {frame[3*i], frame[3*i+1], frame[3*i+2]};
            byte color  = findColor(trueColor);
            if(color >= getPersistenceLevel())
            {
                totalR[color] += trueColor.r;
                totalG[color] += trueColor.g;
                totalB[color] += trueColor.b;
                numbers[color] += 1;
            }
        }

        for(unsigned short color = getPersistenceLevel(); color < numberOfColors; color++)
        {
            colorTable[color] = {static_cast<byte>(std::floor(totalR[color]/numbers[color])), 
                                    static_cast<byte>(std::floor(totalG[color]/numbers[color])),
                                    static_cast<byte>(std::floor(totalB[color]/numbers[color]))};
        }
    }

    return 0;
}