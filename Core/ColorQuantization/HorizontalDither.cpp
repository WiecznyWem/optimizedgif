#include "HorizontalDither.h"

HorizontalDither::HorizontalDither( const IQuantizationMethod& qmethod, 
                    const unsigned short width, 
                    const unsigned short height, 
                    const byte* frame,
                    const FrameCollector* fc)
    : IDitherMethod(qmethod, width, height, frame, fc)
{
    
}

int HorizontalDither::dither()
{
    coloredImage = std::vector<byte>();
    coloredImage.reserve(width*height);

    int suggestR = 0;
    int suggestG = 0;
    int suggestB = 0;

    for(unsigned int i = 0; i < width * height; i++)
    {
        RGBColor trueColor = {frame[3*i], frame[3*i+1], frame[3*i+2]};

        if(suggestR + trueColor.r > 255)
            trueColor.r = 255;
        else if(suggestR + trueColor.r < 0)
            trueColor.r = 0;
        else
            trueColor.r += suggestR;

        if(suggestG + trueColor.g > 255)
            trueColor.g = 255;
        else if(suggestG + trueColor.g < 0)
            trueColor.g = 0;
        else
            trueColor.g += suggestG;

        if(suggestB + trueColor.b > 255)
            trueColor.b = 255;
        else if(suggestB + trueColor.b < 0)
            trueColor.b = 0;
        else
            trueColor.b += suggestB;

        byte suggestedColor = qmethod.findColor(trueColor);
        coloredImage.push_back(suggestedColor);
    
        suggestR = (int)trueColor.r  - (int)qmethod.getColorTable()[suggestedColor].r;
        suggestG = (int)trueColor.g  - (int)qmethod.getColorTable()[suggestedColor].g;
        suggestB = (int)trueColor.b  - (int)qmethod.getColorTable()[suggestedColor].b;
    }

    return 0;
}