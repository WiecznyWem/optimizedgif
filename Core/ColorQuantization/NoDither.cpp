#include "NoDither.h"

NoDither::NoDither( const IQuantizationMethod& qmethod, 
                    const unsigned short width, 
                    const unsigned short height, 
                    const byte* frame,
                    const FrameCollector* fc)
    : IDitherMethod(qmethod, width, height, frame, fc)
{
    
}

int NoDither::dither()
{
    for(unsigned int i = 0; i < width * height; i++)
    {
        RGBColor trueColor = {frame[3*i], frame[3*i+1], frame[3*i+2]};
        coloredImage.push_back(qmethod.findColor(trueColor));
    }

    return 0;
}